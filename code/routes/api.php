<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*
$attributes = [
    'namespace' => 'Api',
    'as' => 'api.',
];



//免认证部分路由
Route::group($attributes, function ($router) {
    $router->resource('test', 'TestController');//测试专用
    $router->resource('cate', 'CateController');//
    $router->resource('homepage', 'HomePageController');//
    $router->resource('items', 'ItemsController');//
    $router->get('homepage-cate-index', 'HomePageController@cate_index')->name('homepage-cate-index');//
    $router->get('homepage-cheap-goods', 'HomePageController@cheap_goods')->name('homepage-cheap-goods');//
    $router->get('cate-list', 'CateController@cate_list')->name('cate-list');//

    $router->post('auth/sms-verify-code', 'AuthenticateController@smsVerifyCode')->name('sms-verify-code');
    $router->post('auth/login-phone', 'AuthenticateController@loginByPhone')->name('login-by-phone');

});
*/

/*--------------------------------------------------------------------------------------------------------------*/
//需要认证部分路由
/*
$attributes['middleware'] = ['auth:api'];

Route::group($attributes, function ($router) {
    $router->get('api-test', 'TestController@apiTest');

    $router->resource('topic', 'TopicController');
    $router->resource('collectAndViewhistory', 'CollectAndViewhistoryController');
    $router->resource('comment', 'CommentController');
    $router->resource('like', 'LikeController');
    $router->resource('user', 'UserController');
    $router->resource('sign-in', 'SignInController');
    $router->get('profile', 'UserController@profile')->name('user.profile');
    $router->resource('user-address', 'UserAddressController');
    $router->get('about', 'OtherController@about')->name('about');

});
*/