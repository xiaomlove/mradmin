<?php

use Illuminate\Database\Seeder;

class ItemsCateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data_file = resource_path("data/shop_cate.json");
        if(file_exists($data_file))
        {
            $data_file = json_decode(file_get_contents($data_file),true);
            foreach ($data_file['data'] as $k=>$v)
            {
                //parent cate
                $parent_data = [
                    'id'=>$v['cid'],
                    'title'=>$v['name'],
                    'cate_img'=>in_array($v['name'],\App\Models\ItemsCate::RECOMMEND_CATE) ? \App\Models\ItemsCate::RECOMMEND_CATEIMAGE[$v['name']] : '',
                    'parent_id'=>0,
                    'order'=>0,
                    'is_recommend'=>in_array($v['name'],\App\Models\ItemsCate::RECOMMEND_CATE) ? 1 : 0,
                ];
                $parent_data_insert = \App\Models\ItemsCate::create($parent_data);
                //subclass data
                $subclass_data = $v['sub_class'];
                if(!empty($subclass_data))
                {
                    foreach ($subclass_data as $kk=>$vv)
                    {
                        \App\Models\ItemsCate::create([
                            'id'=>$vv['jump_value'],
                            'title'=>$vv['name'],
                            'cate_img'=>$vv['icon'],
                            'parent_id'=>$parent_data_insert->id,
                            'order'=>0,
                        ]);
                    }
                }
            }
        }
    }
}
