<?php

use Illuminate\Database\Seeder;
use App\Models\Banner;


class BannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Banner::class)->times(3)->make()->each(function ($banner)  {
            $banner->save();
        });
        //追加分类轮播
        $is_recommend_cate = \App\Models\ItemsCate::where("is_recommend",1)->pluck('id','title')->toArray();
        if(!empty($is_recommend_cate))
        {
            foreach ($is_recommend_cate as $k=>$v)
            {
                Banner::create([
                    'images' => '9c2ea3ab2295f86b5de2f8ba1b1dafdb.png',
                    'title' => $k,
                    'type'=>2,
                    'url'=>"http://www.biadu.com",
                    'position'=>$v,
                ]);
            }
        }
    }
}
