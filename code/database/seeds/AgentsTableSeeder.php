<?php

use Illuminate\Database\Seeder;
use App\Models\Agent;
use App\Models\Admin;
use App\Models\District;

class AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces = District::where('parent_id', 0)->get();
        Admin::where("username", '!=', 'admin')->get()->each(function ($admin) use ($provinces) {
            $admin->agent()->create([
                'name' => "代理商" . $admin->name,
                'level' => 1,
            ]);
        });

    }

    private function getDistrict($provinces)
    {
        $province = $provinces->random();
        $city = $province->cities()->get();
        if ($city->isEmpty())
        {
            echo "can't get city of {$province->name}, again \n";
            return $this->getDistrict($provinces);
        }
        $city = $city->random();

        $district = $city->districts()->get();
        if ($district->isEmpty())
        {
            echo "can't get district of {$city->name}, again \n";
            return $this->getDistrict($provinces);
        }
        $district = $district->random();

        echo sprintf("%s-%s-%s \n", $province->name, $city->name, $district->name);
        return [
            'province_code' => $province->code,
            'city_code' => $city->code,
            'district_code' => $district->code,
        ];
    }
}
