<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index();
            $table->integer('type')->index();
            $table->integer('score')->comment('分值');
            $table->integer('total')->nullable()->comment('当前总数');
            $table->string('remarks')->nullable()->comment('简略备注');
            $table->text('info')->nullable()->comment('其他相关信息');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scores');
    }
}
