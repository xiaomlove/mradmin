<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsSendLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_send_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->comment('用户UID');
            $table->string('phone')->comment('手机号');
            $table->string('code')->comment('验证码');
            $table->dateTime('expires_at')->comment('过期时间');
            $table->dateTime('used_at')->nullable()->comment('使用时间');
            $table->text('send_result_info')->nullable()->comment('发送结果信息');
            $table->tinyInteger('send_result')->comment('发送结果，0成功，1失败');
            $table->timestamps();

            $table->index('uid');
            $table->index('phone');
            $table->index('code');
            $table->index('expires_at');
            $table->index('used_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_send_logs');
    }
}
