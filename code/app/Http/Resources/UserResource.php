<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isRouteProfile = \Route::currentRouteName() == 'api.user.profile';
        return [
            'id' => $this->id,
            'name' => $this->name,
            'nickname' => $this->nickname,
            'scores' => $this->scores,
            'avatar' => imageUrl($this->avatar),
//            'has_sign_in' => $this->when($isRouteProfile, $this->hasSignInToday()),
        ];
    }
}
