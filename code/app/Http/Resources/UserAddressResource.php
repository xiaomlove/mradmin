<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'province_code' => $this->province_code,
            'province' => $this->whenLoaded('province', new DistrictResource($this->province)),
            'city_code' => $this->city_code,
            'city' => $this->whenLoaded('city', new DistrictResource($this->city)),
            'district_code' => $this->district_code,
            'district' => $this->whenLoaded('district', new DistrictResource($this->district)),
            'address' => $this->address,
            'phone' => $this->phone,
        ];
    }
}
