<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'images' => $this->getImagesUrl($this->images),
            'like_count' => $this->like_count,
            'reply_count' => $this->reply_count,
            'floor_num' => $this->floor_num,
            'user' => new UserResource($this->user),
            'created_at' => createdAt($this->created_at),
            'apply_job' => new ApplyJobResource($this->whenLoaded('applyJob')),
        ];
    }

    private function getImagesUrl($imageJson)
    {
        $images = [];
        foreach ((array)json_decode($imageJson, true) as $item)
        {
            $images[] = imageUrl($item);
        }
        return $images;
    }

}
