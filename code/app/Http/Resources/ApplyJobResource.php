<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplyJobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'intention' => $this->intention,
            'province' => new DistrictResource($this->whenLoaded('province')),
            'city' => new DistrictResource($this->whenLoaded('city')),
            'district' => new DistrictResource($this->whenLoaded('district')),
            'phone' => $this->phone,
        ];
    }
}
