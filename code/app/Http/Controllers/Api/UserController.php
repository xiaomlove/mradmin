<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = User::findOrFail($id);

        $resource = new UserResource($result);

        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $update = $request->only(['name', 'nickname', 'avatar', 'password']);
        $update = array_filter($update);
        if (!empty($update['password'])) {
            $update['password'] = bcrypt($update['password']);
        }
        //dd($update);
        $user->update($update);

        return api(RET_OK, RET_SUCCESS_MSG, $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profile()
    {
        $result = \Auth::user();

        $resource = new UserResource($result);

        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }
}
