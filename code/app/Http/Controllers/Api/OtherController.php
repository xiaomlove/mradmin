<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Config;

class OtherController extends Controller
{
    public function about()
    {
        $about = Config::getAbout();

        $data = [
            'content' => (string)$about,
        ];

        return api(RET_OK, RET_SUCCESS_MSG, $data);
    }
}
