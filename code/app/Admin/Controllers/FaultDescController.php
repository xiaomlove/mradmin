<?php

namespace App\Admin\Controllers;

use App\Models\FaultDesc;
use App\Http\Controllers\Controller;
use App\Models\StopReason;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class FaultDescController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        $stopReason = $this->getStopReason();
        $walk = $stopReason->walkThroughRoot($stopReason);
        $path = implode(' -> ', array_reverse(array_column($walk, 'name')));
        return $content
            ->header($path)
            ->description('下的故障')
            ->body($this->grid($stopReason));
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $path = $this->getWalkPath();
        return $content
            ->header($path)
            ->description('下的故障')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        $path = $this->getWalkPath();
        return $content
            ->header($path)
            ->description('下的故障')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($stopReason)
    {
        $grid = new Grid(new FaultDesc);

        $grid->model()->with(['stopReason'])->where('stopreasonId', $stopReason->id);
        $grid->setRelation($stopReason->faultDescriptions());

        $grid->id('Id');
        $grid->code('故障代码');
        $grid->name('故障名称');

        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(FaultDesc::findOrFail($id));

        $show->id('Id');
        $show->code('Code');
        $show->name('Name');
        $show->stopreasonId('StopreasonId');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new FaultDesc);
        $stopReason = $this->getStopReason();
        $form->text('code', '故障代码')->rules(['required']);
        $form->text('name', '故障名称')->rules(['required']);
        $form->hidden('stopreasonId', 'StopreasonId')->value($stopReason->id);

        return $form;
    }

    private function getStopReason()
    {
        return StopReason::findOrFail(request('stopreasonId'));
    }

    private function getWalkPath()
    {
        $stopReason = $this->getStopReason();
        $walk = $stopReason->walkThroughRoot($stopReason);
        $path = implode(' -> ', array_reverse(array_column($walk, 'name')));
        return $path;
    }
}
