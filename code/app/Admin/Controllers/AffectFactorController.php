<?php

namespace App\Admin\Controllers;

use App\Models\AffectFactor;
use App\Http\Controllers\Controller;
use App\Models\AlarmMonitor;
use App\Models\Productline;
use App\Models\StopReason;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AffectFactorController extends Controller
{
    use HasResourceActions;

    protected $header = '三大指标影响因子';

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header($this->header)
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header($this->header)
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $productline = $this->getProductLine();
        return $content
            ->header($this->header)
            ->description('description')
            ->body($this->form($productline)->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        $productline = $this->getProductLine();
        return $content
            ->header($this->header)
            ->description('description')
            ->body($this->form($productline));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $productline = $this->getProductline();
        $grid = new Grid(new AffectFactor);
        $grid->setRelation($productline->affectFactors());
        $grid->model()->with(['stopReason', 'productline']);
        $grid->id('Id');
        $grid->name('名称');
        $grid->affectType('影响类型')->display(function () {
            return $this->affectTypeName;
        });
        $grid->pindex('指标')->display(function () {
            return $this->pindexName;
        });
        $grid->column('stopReason.name', '停机原因');
        $grid->column('productline.name', '生产线');

        $grid->expandFilter();

        $grid->filter(function ($filters) use ($productline) {
            $filters->disableIdFilter();
            $filters->equal('productlineId', '生产线')->select(Productline::pluck('name', 'id'))->default($productline->id);
        });

        $grid->actions(function ($actions) use ($productline) {
            $actions->disableEdit();
            $actions->disableView();
            $actions->append(sprintf('<a href="%s">&nbsp;&nbsp;编辑</a>', route('admin.affect-factor.edit', ['id' => $actions->getKey(), 'productlineId' => $productline->id])));
        });

        $grid->disableExport();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AffectFactor::findOrFail($id));

        $show->id('Id');
        $show->productlineId('ProductlineId');
        $show->name('Name');
        $show->affectType('AffectType');
        $show->stopreasonId('StopreasonId');
        $show->pindex('Pindex');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($productline = null)
    {
        if (!$productline) {
            $productline = new Productline();
        }
        $form = new Form(new AffectFactor);
        $form->hidden('productlineId', 'ProductlineId')->value($productline->id);
        $form->html(sprintf('<h2>当前生产线：%s</h2>', $productline->name));
        $form->text('name', '名称');
        $form->radio('affectType', '影响类型')->options(AffectFactor::$affectTypeNames)->default(AffectFactor::AFFECT_TYPE_STOP_REASON);
        $stopReasonOptions = StopReason::selectOptions(function ($query) use ($productline) {
            return $query->where('productlineId', $productline->id);
        });

        $form->select('stopreasonId', '停机原因')->options($stopReasonOptions)
            ->help(sprintf('影响类型为%s时选择', AffectFactor::$affectTypeNames[AffectFactor::AFFECT_TYPE_STOP_REASON]))
            ->rules("required_if:affectType," . AffectFactor::AFFECT_TYPE_STOP_REASON);

        $form->radio('pindex','统计指标')->options(array_only(
                AlarmMonitor::$indexNames, [AlarmMonitor::INDEX_GLY, AlarmMonitor::INDEX_OEE, AlarmMonitor::INDEX_LEF]
            ))->help(sprintf('影响类型为%s时选择', AffectFactor::$affectTypeNames[AffectFactor::AFFECT_TYPE_STAT_INDEX]))
            ->default(AlarmMonitor::INDEX_GLY)
            ->rules("required_if:affectType," . AffectFactor::AFFECT_TYPE_STAT_INDEX);

        $form->tools(function ($tools) use ($productline) {
            $tools->disableList();
            $tools->disableView();
            $tools->append(formListUrl(route('admin.affect-factor.index', ['productlineId' => $productline->id])));
        });

        return $form;
    }

}
