<?php

namespace App\Admin\Controllers;

use App\Models\Machine;
use App\Models\Productline;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Tab;
use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\Request;
use App\Admin\Extensions\ManchineProductline;

class MachineController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('机台一览')
            ->description('description')
            ->body($this->grid());
    }
    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('机台详情')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('编辑机台')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        // print_r(Request::get('productline'));exit();
        return $content
            ->header('添加机台')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $tab = new Tab();

        $productlines = Productline::pluck('id')->toArray();
        // print_r($productlines);exit();
        $grid = new Grid(new Machine);
        if (in_array(Request::get('productline'), $productlines)) {
            $grid->model()->where('produceLineId', Request::get('productline'));
        }
        $grid->id('Id');
        $grid->column('graphic','可视化图形')->display(function() {
            if (empty($this->graphic)) {
                return '---';
            }else{
                $url = graphicImageUrl($this->graphic);
                return sprintf('<img src="%s" class="img-sm" />', $url);
            }
        });
        $grid->name('机台名称');
        $grid->code('编码');
        $grid->opcGroupName('OPC组地址');
        $grid->sort('排序');
        // $grid->produceLineId('所属生产线');

        $grid->produceLineId('所属生产线')->display(function($produceLineId) {
            $productLineObj = Productline::find($produceLineId);
            return empty($productLineObj) ? '---' : $productLineObj->name;

            // $produceLinename = Productline::find($produceLineId)->name;
            // return $produceLinename;
        });

        $grid->keyMachineType('所属关键机台类型')->display(function($id) {
            $keyMachineType = $this->keyMachineType;
            $keyMachineTypeName = $this->getKeyMachineTypeName();
            return $keyMachineTypeName;
        });

        $grid->type('机台类型')->display(function($id) {
            $type = $this->type;
            $typeName = $this->getTypeName();
            return $typeName;
        });
        $grid->actions(function ($actions) {
            $vals = $actions->row;
            $produceLineId = $actions->row->produceLineId;
            $id = $actions->row->id;
            $url = Request::fullUrlWithQuery(['pId' => $produceLineId,'mId' => $id]);

            $actions->prepend('<a href="/admin/">报警配置</a> | ');
            $mcUrl = str_replace("machine","macfaultconfig",$url);
            $actions->prepend('<a href="'.$mcUrl.'">故障状态配置</a> | ');
            
            $rurl = str_replace("machine","signalpoint",$url);
            $actions->prepend('<a href="'.$rurl.'">点位配置</a> | ');
            // $actions->prepend('<a href="/admin/signalpoint/'.$produceLineId.'/'.$id.'">点位配置</a> | ');
            $actions->disableView();

        });
        $grid->disableCreateButton();
        $grid->disableExport();
        $grid->tools(function ($tools) {
            $productlines = Productline::pluck('name', 'id');
            $tools->append(new ManchineProductline($productlines));
        });

        $tab->add('Pie', $grid);
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Machine::findOrFail($id));

        $show->id('Id');
        $show->opcGroupName('OPC组地址');
        $show->code('编码');
        $show->graphic('可视化图形');
        $show->name('机台名称');
        $show->sort('排序');
        $show->produceLineId('所属生产线');
        $show->keyMachineType('所属关键机台类型');
        $show->type('机台类型');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $form = new Form(new Machine);
        $productline = Request::get('productline');
        if (empty($productline)) {
            $form->select('produceLineId', '所属生产线')->options(Productline::pluck('name', 'id'));
        }else{
            $form->select('produceLineId', '所属生产线')->options(Productline::pluck('name', 'id'))->default($productline);
        }

        
        $form->radio('type', '机台类型')->options(Machine::$typeNames);
        $form->radio('keyMachineType', '所属关键机台类型')->options(Machine::$keyMachineTypeNames);
        $form->text('name', '机台名称');
        $form->text('code', '编码');
        $form->text('opcGroupName', 'OPC组地址');
        // $directors = [
        //     'John'  => 1,
        //     'Smith' => 2,
        //     'Kate'  => 3,
        // ];
        // $form->number('produceLineId', '所属生产线');
        // dd($form);exit();
        $form->image('graphic', "可视化图形")->uniqueName();
        
        // $form->saving(function (Form $form) use ($id) {
        //     dd($form);exit();
        // });
        return $form;
    }
}
