<?php

namespace App\Admin\Controllers;

use App\Models\PalletizerCapacity;
use App\Http\Controllers\Controller;
use App\Models\Productline;
use App\Models\ProductType;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class PalletizerCapacityController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('品种配置')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('品种配置')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('品种配置')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('品种配置')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PalletizerCapacity);
        $grid->model()->with(['productline', 'productType']);

        $grid->id('Id');
        $grid->column('productType.name', '品种');
        $grid->unit('品种配置单位')->display(function () {
            return $this->unitText;
        });
        $grid->boxNumber('每跺箱数');
        $grid->bottleNumber('每箱瓶数');
        $grid->ratedOutput('额定产量(瓶/小时)');
        $grid->column('productline.name', '生产线');

        $grid->disableExport();

        $grid->filter(function ($filters) {
            $filters->equal('productlineId', '生产线')->select(Productline::pluck('name', 'id'));
        });

        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PalletizerCapacity::findOrFail($id));

        $show->id('Id');
        $show->productlineId('ProductlineId');
        $show->productTypeId('ProductTypeId');
        $show->boxNumber('BoxNumber');
        $show->bottleNumber('BottleNumber');
        $show->unit('Unit');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PalletizerCapacity);

        $form->select('productlineId', '生产线')->options(Productline::pluck('name', 'id'));
        $form->select('productTypeId', '品种')->options(ProductType::pluck('name', 'id'));
        $form->number('boxNumber', '每跺箱数');
        $form->number('bottleNumber', '每箱瓶数');
        $form->radio('unit', '单位')->options(PalletizerCapacity::$unitTexts)->default(PalletizerCapacity::UNIT_DUO);
        $form->number('ratedOutput', '额定产量')->help('单位：瓶/小时');

        return $form;
    }
}
