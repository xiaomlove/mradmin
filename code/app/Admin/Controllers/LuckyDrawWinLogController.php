<?php

namespace App\Admin\Controllers;

use App\Models\LuckyDrawWinLog;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class LuckyDrawWinLogController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('中奖')
            ->description('记录')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new LuckyDrawWinLog);
        $prizeId = request('lucky_draw_prize_id');
        $luckyDrawId = request('lucky_draw_id');
        $grid->model()
            ->with(['user', 'luckyDraw', 'luckyDrawPrize'])
            ->when($prizeId, function ($query) use ($prizeId) {
                $query->where('lucky_draw_prize_id', $prizeId);
            })->when($luckyDrawId, function ($query) use ($luckyDrawId) {
                $query->where('lucky_draw_id', $luckyDrawId);
            });

        $grid->id('ID');
        $grid->column('user.username', '用户名');
        $grid->column('user.name', '真实姓名');
        $grid->column('抽奖')->display(function () {
            return $this->luckyDraw->title;
        });
        $grid->column('奖品')->display(function () {
            return $this->luckyDrawPrize->title;
        });
        $grid->created_at('中奖时间');

        $grid->disableActions();
        $grid->disableCreateButton();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(LuckyDrawWinLog::findOrFail($id));



        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new LuckyDrawWinLog);



        return $form;
    }
}
