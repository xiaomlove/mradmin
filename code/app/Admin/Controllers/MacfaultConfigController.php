<?php

namespace App\Admin\Controllers;

use App\Models\MacfaultConfig;
use App\Models\Machine;
use App\Models\Productline;
use App\Models\Signalpoint;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Request;
use App\Admin\Extensions\Manchines;

class MacfaultConfigController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('故障状态配置一览')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('详情故障状态配置')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('编辑故障状态配置')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('添加故障状态配置')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new MacfaultConfig);
        if (Request::get('mId')) {
            $grid->model()->where('machineId', '=', Request::get('mId'));
            // print_r(Request::get('mId'));exit();
        }


        // $grid->id('Id');
        $grid->name('故障名称');
        // $grid->graphic('状态图');
        $grid->column('graphic','状态图')->display(function() {
            if (empty($this->graphic)) {
                return '---';
            }else{
                $url = graphicImageUrl($this->graphic);
                return sprintf('<img src="%s" class="img-sm" />', $url);
            }
        });

        // $grid->color('色块色值');
        $grid->color('色块色值')->display(function($id) {
            return '<span style="display: block; width: 50px; height: 30px; background-color: '.$this->color.';"></span><span>'.$this->color.'</span>';
        });
        // $grid->valueType('故障值类型');

        $grid->valueType('故障值类型')->display(function($id) {
            $valueTypeName = $this->getValueTypeName();
            return $valueTypeName;
        });

        $grid->valueScope('故障值')->display(function($id) {
            $isLimitValue = $this->isLimitValue();
            if ($isLimitValue) {
                return '<span>上限：'.$this->maximum.'</span></br><span>下限：'.$this->minimum.'</span>';
            }else{
                return $this->threshold;
            }
        });

        // $grid->valueScope('故障值');
        $grid->description('描述');
        // $grid->maximum('上限');
        // $grid->minimum('下限');
        // $grid->threshold('阈值');
        // $grid->signalPointId('信号点位ID');
        $grid->disableCreateButton();
        $grid->disableExport();
        $grid->tools(function ($tools) {
            // $productlines = Productline::pluck('name', 'id');
            $activeMId = Request::get('mId');
            $productlineName = Productline::where('id',Request::get('pId'))->value('name');
            $machines = Machine::where('produceLineId',Request::get('pId'))->pluck('name',"id");
            // $url = Request::fullUrlWithQuery(['pId' => Request::get('pId'),'mId' => Request::get('mId')]);
            $creatUrl = Request::url().'/create?pId='.Request::get('pId').'&mId='.Request::get('mId');

            $tools->append(new Manchines($productlineName,$machines,$creatUrl));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(MacfaultConfig::findOrFail($id));
        

        $show->id('Id');
        $show->machineId('机台ID');
        $show->description('描述');
        $show->name('名称');
        $show->valueType('值类型');
        $show->maximum('上限');
        $show->minimum('下限');
        $show->threshold('阈值');
        $show->graphic('状态图');
        $show->color('色块色值');
        $show->signalPointId('信号点位ID');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new MacfaultConfig);
        $productlineId = Request::get('pId');
        $machineId = Request::get('mId');
        if (!empty($machineId)) {
            // $form->select('productlineId', '所属生产线')->options(Productline::where('id',$productlineId)->pluck('name', 'id'))->default($productlineId)->setWidth(3, 3);
            $form->display('productlineId', '所属生产线')->value(Productline::where('id',$productlineId)->value('name')); 
            $form->display('machineId', '所属机台')->value(Machine::where('id',$machineId)->value('name')); 
            // $form->select('machineId', '所属机台')->options(Machine::where('produceLineId',$productlineId)->pluck('name', 'id'))->default($machineId);
        }else{
            // $form->select('productlineId', '所属生产线')->options(Productline::pluck('name', 'id'))->load('machineId', '/signalpoint/getMachines');
            // $form->select('machineId', '所属机台')->options(Machine::pluck('name', 'id'));
        }
        $form->text('name', '名称');
        $form->select('signalPointId', '点位')->options(Signalpoint::where('machineId',$machineId)->pluck('name', 'id'));
        // $form->number('valueType', '值类型');
        $form->radio('valueType', '值类型')->options(MacfaultConfig::$valueTypeNames);


        // $form->number('machineId', '机台ID');
        
        $form->decimal('maximum', '上限');
        $form->decimal('minimum', '下限');
        $form->text('threshold', '阈值');
        $form->color('color', '色块色值');
        $form->textarea('description', '描述');
        // $form->text('graphic', '状态图');
        $form->image('graphic', "状态图")->uniqueName();
        // $form->hidden('productlineId', $productlineId)->value($productlineId);
        $form->hidden('machineId', $machineId)->value($machineId);
        $form->hidden('faultType','产生故障的原因类型');
        // $signalPoint = Signalpoint::where('id',1)->value('type');
        // print_r($signalPoint);exit();
        $form->saving(function (Form $form) {
            $signalPointId = $form->model()->signalPointId;
            $signalPointType = Signalpoint::where('id',$signalPointId)->value('type');
            // $signalPointType = $signalPoint->type;
            $faultType = MacfaultConfig::getFaultType($signalPointType);
            $form->faultType = $faultType;
            // dump($form->username);
        });
        // $form->number('signalPointId', '信号点位ID');
        return $form;
    }
}
