<?php

namespace App\Admin\Controllers;

use App\Models\Factory;
use App\Models\ProductType;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ProductTypeController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('品种')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('品种')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('品种')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('品种')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ProductType);
        $grid->model()->with(['factory']);

        $grid->id('Id');
        $grid->number('编号');
        $grid->name('名称');
        $grid->shortName('简称');
        $grid->stateColor('状态颜色');
        $grid->column('factory.name', '工厂')->label('info');

        $grid->filter(function ($filters) {
            $filters->equal('factoryId', '工厂')->select(Factory::pluck('name', 'id'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ProductType::findOrFail($id));

        $show->id('Id');
        $show->number('Number');
        $show->name('Name');
        $show->shortName('ShortName');
        $show->ratedOutput('RatedOutput');
        $show->factoryId('FactoryId');
        $show->stateColor('StateColor');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ProductType);

        $form->text('number', '编号');
        $form->text('name', '名称');
        $form->text('shortName', '简称');
        $form->color('stateColor', '状态颜色');
        $form->select('factoryId', '工厂')->options(Factory::pluck('name', 'id'));

        return $form;
    }
}
