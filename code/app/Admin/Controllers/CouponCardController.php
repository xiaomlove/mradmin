<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\BatchHandle;
use App\Models\CouponCard;
use App\Http\Controllers\Controller;
use App\Models\CouponCardCreateLog;
use App\User;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;


class CouponCardController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('优惠卡')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('优惠卡')
            ->description('详情')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('优惠卡')
            ->description('编辑')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('优惠卡')
            ->description('批量生成')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CouponCard);
        $grid->id('ID')->sortable();

        $isSuperAdmin = \Admin::user()->isSuperAdmin();
        $isOfficial = \Admin::user()->isOfficial();
        $isAgentLevelOne = \Admin::user()->isAgentLevelOne();
        $isAgentLevelTwo = \Admin::user()->isAgentLevelTwo();
        $cur_login_admin_map_agent_id = \Admin::user()->agent()->first()->id ?? 0;
        if(!$isSuperAdmin && !$isOfficial)
        {
            if($isAgentLevelOne)
            {
                $grid->model()->where("first_class_agent_id",$cur_login_admin_map_agent_id);
            }
            if($isAgentLevelTwo)
            {
                $grid->model()->where("second_class_agent_id",$cur_login_admin_map_agent_id);
            }
        }


        $grid->coupon_card_no('优惠卡编号');
        $grid->coupon_card_pub_key('优惠卡帐号');
        $grid->coupon_card_pri_key('优惠卡密码');
//        $grid->first_class_agent_id('一级代理商');
        $grid->column('belongsToLevelOneAgent.name',"一级代理商");
        $grid->column('belongsToLevelTwoAgent.name',"二级代理商");
//        $grid->second_class_agent_id('二级代理商');
        $grid->is_used('是否使用')->display(function ($is_used) {
            $color = $is_used ? "red" : "green";
            $text = $is_used ? "已使用" : "未使用";
            return "<span style='color: $color'>$text</span>";
        });
        $grid->points_quota('积分额度');
        $grid->column('belongsToUser.name',"使用用户");
        $grid->column('belongsToUser.phone',"使用用户手机");
        $grid->created_at('创建时间');
        $grid->updated_at('更新时间');

        $grid->filter(function ($filters) {
            $filters->equal('coupon_card_no', '优惠卡编号');
            $filters->equal('coupon_card_pub_key', '优惠卡帐号');
            $filters->equal('is_used', '是否使用')->select([1=>"是",0=>"否"]);

            $filters->where(function ($query) {
                $query->whereHas("belongsToUser", function ($query) {
                    $query->where("bind_uid", $this->input);
                });
            }, "使用者", "belongsToUser")->select(User::pluck("name","id"));

            $filters->where(function ($query) {
                $query->whereHas("assignAdmin", function ($query) {
                    $query->where("assign_admin_uid", $this->input);
                });
            }, "分配管理员", "assignAdmin")->select(Administrator::pluck("name","id"));

            $filters->where(function ($query) {
                switch ($this->input) {
                    case 'first':
                        $query->has('belongsToLevelOneAgent');
                        break;
                    case 'secoend':
                        $query->has('belongsToLevelTwoAgent');
                        break;
                }
            }, '代理商', 'agent')->radio([
                '' => '所有',
                'first' => '一级代理商',
                'secoend' => '二级代理商',
            ]);

        });

        $grid->disableCreateButton();
        $grid->actions(function ($actions) {
            $actions->disableEdit();
            $actions->disableDelete();
        });

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
                $batch->add('删除', new BatchHandle("CouponCard","","","batchHandle",true));
            });
        });


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CouponCard::findOrFail($id));

        $show->id('Id');
        $show->coupon_card_no('优惠卡编号');
        $show->coupon_card_pub_key('优惠卡帐号');
        $show->coupon_card_pri_key('优惠卡密码');
        $show->first_class_agent_id('一级代理商');
        $show->second_class_agent_id('二级代理商');

        $show->is_used('是否使用')->as(function ($value) {
            return $value ? "是" : "否";
        });

        $show->use_username('使用用户')->as(function () {
            return User::find($this->bind_uid) ? User::find($this->bind_uid)->name : "";
        });
        $show->use_userphone('使用用户手机')->as(function () {
            return User::find($this->bind_uid) ? User::find($this->bind_uid)->phone : "";
        });

        $show->points_quota('积分额度');
        $show->created_at('创建时间');
        $show->updated_at('更新时间');
        $show->panel()->tools(function ($tools) {
            $tools->disableEdit();
            $tools->disableDelete();
        });
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CouponCard);
        $form->number('points_quota_input', '积分额度')->default(1);
        $form->number('create_nums_input', '批量创建数量')->default(1);
        $form->hidden("create_admin_uid","操作管理员uid")->value(Admin::user()->id);

        return $form;
    }
}
