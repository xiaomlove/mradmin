<?php

namespace App\Admin\Controllers;

use App\Models\LuckyDraw;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class LuckyDrawController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('抽奖')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new LuckyDraw);

        $grid->id('Id');
        $grid->title('标题');
        $grid->cover('封面');
        $grid->consume_score('消耗积分');
        $grid->begin_time('开始时间');
        $grid->end_time('结束时间');

        $grid->actions(function ($actions) {
            $actions->append(sprintf('<a href="%s" target="_blank">&nbsp;&nbsp;奖品</a>', route('admin.lucky-draw-prize.index', ['lucky_draw_id' => $actions->getKey()])));
            $actions->append(sprintf('<a href="%s" target="_blank">&nbsp;&nbsp;中奖记录</a>', route('admin.lucky-draw-win-log.index', ['lucky_draw_id' => $actions->getKey()])));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(LuckyDraw::findOrFail($id));

        $show->begin_time('Begin time');
        $show->consume_score('Consume score');
        $show->cover('Cover');
        $show->created_at('Created at');
        $show->description('Description');
        $show->end_time('End time');
        $show->id('Id');
        $show->title('Title');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new LuckyDraw);

        $form->text('title', '标题')->rules(['required']);
        $form->image('cover', '封面图');
        $form->textarea('description', '描述信息');
        $form->number('consume_score', '消耗积分');
        $form->datetime('begin_time', '开始时间')->rules(['required']);
        $form->datetime('end_time', '结束时间')->rules(['required', 'after:' . request('begin_time')]);

        return $form;
    }
}
