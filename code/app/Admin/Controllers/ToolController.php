<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Encore\Admin\Layout\Column;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\Form;
use Illuminate\Support\MessageBag;

use App\User;

class ToolController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Token生成');
            $content->description('');
            $content->row(function (Row $row) {
                $row->column(4, function (Column $column) {
                    $form = new Form();
                    $form->action('/admin/tool/token');
                    $form->select('uid', '用户')->ajax('/admin/user/select/json');
                    $column->append($form->render());
                });
            });

        });
    }

    public function token(Request $request)
    {
        $uid = $request->uid;
        $user = User::find($uid);
        if (!$user)
        {
            $error = new MessageBag(['title' => '参数错误', 'message' => '需要选择一个用户']);
            return back()->with(compact('error'));
        }
        $tokenResult = $user->createToken('后台生成');
        $success = new MessageBag(['title' => '成功', 'message' => $tokenResult->accessToken]);
        return back()->with(compact('success'));
    }
}
