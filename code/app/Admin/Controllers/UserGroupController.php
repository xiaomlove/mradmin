<?php

namespace App\Admin\Controllers;

use App\Models\Productline;
use App\Models\UserGroup;
use App\Http\Controllers\Controller;
use App\User;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class UserGroupController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('班组')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('班组')
            ->description('信息')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('班组')
            ->description('班组')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('班组')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new UserGroup);
        $grid->model()->with(['productline']);

        $grid->id('Id');
        $grid->name('名称');
        $grid->description('描述');
        $grid->stateColor('状态图颜色');
        $grid->column('productline.name', '所属生产线')->label('info');

        $grid->filter(function ($filters) {
            $filters->equal('productlineId', '所属生产线')->select(Productline::pluck('name', 'id'));
        });

        $grid->actions(function ($actions) {
            $url = route('admin.member-of-user-group.index', ['groupId' => $actions->getKey()]);
            $actions->append(sprintf('<a href="%s" target="_blank">&nbsp;&nbsp;成员</a>', $url));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UserGroup::findOrFail($id));

        $show->id('Id');
        $show->identifier('Identifier');
        $show->name('Name');
        $show->parentId('ParentId');
        $show->addTime('AddTime');
        $show->roleId('RoleId');
        $show->description('Description');
        $show->stateColor('StateColor');
        $show->productlineId('ProductlineId');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UserGroup);

        $form->text('name', '名称');
        $form->textarea('description', '描述');
        $form->color('stateColor', '状态图颜色');
        $form->select('productlineId', '所属生产线')->options(Productline::pluck('name', 'id'));

        return $form;
    }
}
