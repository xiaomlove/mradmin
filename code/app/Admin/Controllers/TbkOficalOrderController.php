<?php

namespace App\Admin\Controllers;

use App\Models\TbkOficalOrder;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class TbkOficalOrderController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new TbkOficalOrder);

        $grid->id('Id');

        $grid->trade_id('淘宝订单号');
        $grid->trade_parent_id('淘宝父订单号');
        $grid->alipay_total_price('付款金额');
        $grid->auction_category('类目名称');
        $grid->commission('佣金');
        $grid->create_time('下单时间');
        $grid->item_num('商品数量');
        $grid->item_title('商品标题')->display(function ($value){
            if(empty($this->num_iid)) return "";
            else
            {
                $goods_url = sprintf('https://item.taobao.com/item.htm?id=%s',$this->num_iid);
                return "<a href='$goods_url' target='_blank'>$value</a>";
            }
        });
        $grid->order_type('订单类型');
        $grid->pay_price('实际支付金额');
        $grid->price('单价');
        $grid->seller_nick('卖家昵称');
        $grid->seller_shop_title('卖家店铺名称');
        $grid->terminal_type('成交平台')->display(function ($value){
            if($value == 1) return "PC";
            elseif ($value == 2) return '无线';
            else return "未知";
        });
        $grid->tk_status('订单状态')->display(function ($value){
            switch ($value)
            {
                case 3:
                    return '订单结算';
                case 12:
                    return '订单付款';
                case 13:
                    return '订单失效';
                case 14:
                    return '订单成功';
            }
        });
        //$grid->created_at('Created at');
        //$grid->updated_at('Updated at');

        $grid->disableExport();
        $grid->disableCreation();
        $grid->disableFilter();
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });
        $grid->actions(function ($actions) {
            $actions->disableEdit();
            $actions->disableDelete();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(TbkOficalOrder::findOrFail($id));

        $show->id('Id');
        $show->adzone_id('Adzone id');
        $show->adzone_name('Adzone name');
        $show->alipay_total_price('Alipay total price');
        $show->auction_category('Auction category');
        $show->click_time('Click time');
        $show->commission('Commission');
        $show->commission_rate('Commission rate');
        $show->create_time('Create time');
        $show->income_rate('Income rate');
        $show->item_num('Item num');
        $show->item_title('Item title');
        $show->num_iid('Num iid');
        $show->order_type('Order type');
        $show->pay_price('Pay price');
        $show->price('Price');
        $show->pub_share_pre_fee('Pub share pre fee');
        $show->seller_nick('Seller nick');
        $show->seller_shop_title('Seller shop title');
        $show->site_id('Site id');
        $show->site_name('Site name');
        $show->subsidy_rate('Subsidy rate');
        $show->subsidy_type('Subsidy type');
        $show->terminal_type('Terminal type');
        $show->tk3rd_type('Tk3rd type');
        $show->tk_status('Tk status');
        $show->total_commission_rate('Total commission rate');
        $show->trade_id('Trade id');
        $show->trade_parent_id('Trade parent id');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new TbkOficalOrder);

        $form->number('adzone_id', 'Adzone id');
        $form->text('adzone_name', 'Adzone name');
        $form->decimal('alipay_total_price', 'Alipay total price')->default(0.00);
        $form->text('auction_category', 'Auction category');
        $form->datetime('click_time', 'Click time')->default(date('Y-m-d H:i:s'));
        $form->decimal('commission', 'Commission')->default(0.00);
        $form->decimal('commission_rate', 'Commission rate')->default(0.00);
        $form->datetime('create_time', 'Create time')->default(date('Y-m-d H:i:s'));
        $form->decimal('income_rate', 'Income rate')->default(0.00);
        $form->number('item_num', 'Item num');
        $form->text('item_title', 'Item title');
        $form->number('num_iid', 'Num iid');
        $form->text('order_type', 'Order type');
        $form->decimal('pay_price', 'Pay price')->default(0.00);
        $form->decimal('price', 'Price')->default(0.00);
        $form->decimal('pub_share_pre_fee', 'Pub share pre fee')->default(0.00);
        $form->text('seller_nick', 'Seller nick');
        $form->text('seller_shop_title', 'Seller shop title');
        $form->number('site_id', 'Site id');
        $form->text('site_name', 'Site name');
        $form->decimal('subsidy_rate', 'Subsidy rate')->default(0.00);
        $form->switch('subsidy_type', 'Subsidy type');
        $form->switch('terminal_type', 'Terminal type');
        $form->text('tk3rd_type', 'Tk3rd type');
        $form->switch('tk_status', 'Tk status');
        $form->decimal('total_commission_rate', 'Total commission rate')->default(0.00);
        $form->text('trade_id', 'Trade id');
        $form->text('trade_parent_id', 'Trade parent id');

        return $form;
    }
}
