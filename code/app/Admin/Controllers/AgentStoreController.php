<?php

namespace App\Admin\Controllers;

use App\Models\Agent;
use App\Models\AgentStore;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AgentStoreController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AgentStore);
        $grid->model()->with(['agent', 'province', 'city', 'district']);

        $grid->name('名称');
        $grid->column('agent.name', '所属代理商');
        $grid->column('province.name', '省');
        $grid->column('city.name', '市');
        $grid->column('district.name', '区');
        $grid->address('详细地址');
        $grid->phone('联系电话');
        $grid->cover('封面')->display(function ($value) {
            $thumb = imageUrl($this->cover, 'resize,w_30,h_30');
            $origin = imageUrl($this->cover);
            return sprintf('<a href="%s" target="_blank"><img src="%s" class="img-sm" /></a>', $origin, $thumb);
        });
        $grid->remarks('备注');

        $grid->filter(function ($filters) {
            $filters->like('name', '名称');
            $filters->equal('agent_id', '代理商')->select(Agent::pluck('name', 'id'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AgentStore::findOrFail($id));

        $show->address('Address');
        $show->agent_id('Agent id');
        $show->city_code('City code');
        $show->cover('Cover');
        $show->created_at('Created at');
        $show->district_code('District code');
        $show->id('Id');
        $show->images('Images');
        $show->latitude('Latitude');
        $show->longitude('Longitude');
        $show->name('Name');
        $show->phone('Phone');
        $show->province_code('Province code');
        $show->remarks('Remarks');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AgentStore);

        $form->select('agent_id', '代理商')->options(Agent::pluck('name', 'id'));
        $form->text('name', '名称')->rules(['required']);
        $form->distpicker(['province_code' => '省', 'city_code' => '市', 'district_code' => '区'], '位置')->rules(['required']);
        $form->text('address', '具体地址')->rules(['required']);
        $form->mobile('phone', '联系手机');
        $form->image('cover', '封面')->uniqueName()->rules(['image']);
        $form->multipleImage('images', '图片')->uniqueName()->removable()->rules(['image']);
        $form->map('latitude', 'longitude', '坐标');
        $form->text('remarks', '备注');

        return $form;
    }
}
