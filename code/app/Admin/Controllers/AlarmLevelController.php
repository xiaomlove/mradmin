<?php

namespace App\Admin\Controllers;

use App\Models\AlarmLevel;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AlarmLevelController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AlarmLevel);

        $grid->id('Id');
        $grid->name('名称');
        $grid->icon('图标')->display(function () {
            return sprintf('<img src="%s" />', asset("img/alarm-level/" . $this->icon));
        });
        $grid->description('描述');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AlarmLevel::findOrFail($id));

        $show->id('Id');
        $show->name('名称');
        $show->icon('图标');
        $show->description('描述');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AlarmLevel);
        $form->text('name', '名称');
        $icons = [];
        foreach (AlarmLevel::$icons as $icon) {
            $icons[$icon] = sprintf('<img src="%s" />', asset('img/alarm-level/' . $icon));
        }
        $form->unEscapeRadio('icon', '图标')->options($icons);
        $form->textarea('description', '描述');

        return $form;
    }
}
