<?php

namespace App\Admin\Controllers;

use App\Models\Admin;
use App\Models\Agent;
use App\Models\CouponCardCreateLog;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Table;

class CouponCardCreateLogController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CouponCardCreateLog);

        $grid->id('Id');
        $grid->column('belongsToAdmin.name', '创建者');
        $grid->points_quota_input('积分额度');
        $grid->create_nums_input('创建数量');

        $grid->column('create_card_start_end','编号起始')->display(function (){
            return $this->create_card_start."--".$this->create_card_end;
            //return '<a  href="'.admin_base_path('couponCardCreateLogDetail').'?log_id='.$this->id.'" target="_blank">明细</a>';
        });

        $grid->created_at('创建时间');
        $grid->updated_at('更新时间');
        $grid->disableActions();
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        $grid->filter(function ($filter) {
            $filter->where(function ($query) {
                $query->whereHas("belongsToAdmin", function ($query) {
                    $query->where("create_admin_uid", $this->input);
                });
            }, "创建者", "belongsToAdmin")->select(Admin::pluck("name","id"));

            $filter->equal('points_quota_input',"积分额度");
            $filter->equal('create_nums_input',"创建数量");
        });

        return $grid;
    }

    /**
     * @desc get card_sets
     * @param $card_sets
     */
    protected function card_sets_datas($card_sets)
    {
        $card_sets = is_array($card_sets) ? $card_sets : json_decode($card_sets,true);
        $return_data = [];
        $coupon_card_no_list = $card_sets['coupon_card_no_list'];
        $coupon_card_pub_list = $card_sets['coupon_card_pub_list'];
        $coupon_card_pri_list = $card_sets['coupon_card_pri_list'];

        foreach ($coupon_card_no_list as $k=>$v)
        {
            $return_data[] = [
                $v,
                $coupon_card_pub_list[$k],
                $coupon_card_pri_list[$k]
            ];
        }
        return $return_data;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CouponCardCreateLog::findOrFail($id));

        $show->id('Id');
        $show->points_quota_input('Points quota input');
        $show->create_nums_input('Create nums input');
        $show->create_admin_uid('Create admin uid');
        $show->card_sets('Card sets');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        set_time_limit(0);
        $form = new Form(new CouponCardCreateLog);

        $form->number('points_quota_input', '积分额度')->rules('required|numeric|min:1')->default(1);
        $form->number('create_nums_input', '创建数量')->rules('required|numeric|min:1')->default(1);
        return $form;
    }
}
