<?php

namespace App\Admin\Controllers;

use App\Models\Signalpoint;
use App\Models\Productline;
use App\Models\Machine;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Request;
use App\Admin\Extensions\Manchines;

class SignalpointController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('点位一览')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('点位详情')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('编辑点位')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('添加点位')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     * @return Grid
     */
    protected function grid()
    {   
        $grid = new Grid(new Signalpoint);
        
        if (Request::get('mId')) {
            $grid->model()->where('machineId', '=', Request::get('mId'));
        }

        $grid->name('点位名称');
        $grid->type('点位类型')->display(function($id) {
            $type = $this->type;
            $typeName = $this->getTypeName();
            return $typeName;
        });
        $grid->opcItemName('OPC组地址');
        $grid->defaultValue('常规值');
        $grid->unit('单位');
        $grid->isSample('是否采样')->display(function($id) {
            $isSample = $this->isSample;
            $isSampleName = $this->getSampleTypeName();
            return $isSampleName;
        });
        $grid->sampleRate('采样频率(分钟)');
        $grid->actions(function ($actions) {
            $actions->prepend('<a href="/admin/">报警配置</a> | ');
        });
        // $grid->id('Id');
        // $grid->valType('值类型');
        $grid->disableCreateButton();
        $grid->disableExport();
        $grid->tools(function ($tools) {
            // $productlines = Productline::pluck('name', 'id');
            // $activeMId = Request::get('mId');
            $productlineName = Productline::where('id',Request::get('pId'))->value('name');
            $machines = Machine::where('produceLineId',Request::get('pId'))->pluck('name',"id");
            $createUrl = Request::url().'/create?pId='.Request::get('pId').'&mId='.Request::get('mId');
            $tools->append(new Manchines($productlineName,$machines,$createUrl));
        });
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Signalpoint::findOrFail($id));

        $show->id('Id');
        $show->opcItemName('OPC分组名称');
        $show->unit('单位');
        $show->name('点位名称');
        $show->defaultValue('常规值');
        $show->type('点位类型');
        $show->valType('值类型');
        $show->productlineId('所属生产线');
        $show->machineId('所属机台');
        $show->isSample('是否采样');
        $show->sampleRate('采样频率(分钟)');
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Signalpoint);
        $productlineId = Request::get('pId');
        $machineId = Request::get('mId');


        if (!empty($productlineId)) {
            // $form->select('productlineId', '所属生产线')->options(Productline::where('id',$productlineId)->pluck('name', 'id'))->default($productlineId)->setWidth(3, 3);
            $form->display('productlineId', '所属生产线')->value(Productline::where('id',$productlineId)->value('name'))->setWidth(3, 3); 
            $form->select('machineId', '所属机台')->options(Machine::where('produceLineId',$productlineId)->pluck('name', 'id'))->default($machineId)->setWidth(3, 3);
        }else{
            $form->select('productlineId', '所属生产线')->options(Productline::pluck('name', 'id'))->load('machineId', '/signalpoint/getMachines')->setWidth(3, 3);
            $form->select('machineId', '所属机台')->options(Machine::pluck('name', 'id'))->setWidth(3, 3);
        }
        
        $form->radio('type', '点位类型')->options(Signalpoint::$typeNames)->setWidth(9, 3);
        $form->text('name', '点位名称')->setWidth(3, 3);
        $form->text('opcItemName', 'OPC组地址')->setWidth(3, 3);
        $form->radio('valType', '值类型')->options(Signalpoint::$valTypeNames)->setWidth(6, 3);
        $form->text('defaultValue', '常规值')->setWidth(3, 3);
        $form->text('unit', '单位')->setWidth(3, 3);
        $form->radio('isSample', '是否采样')->options(Signalpoint::$sampleTypeNames)->setWidth(3, 3);
        $sampleRate = [
            5 => '5分钟',
            10 => '10分钟',
            30 => '30分钟',
            60 => '1小时',
        ];
        $form->radio('sampleRate', '采样频率')->options($sampleRate)->default(5)->setWidth(6, 3);

        $form->hidden('productlineId', $productlineId)->value($productlineId);

        return $form;
    }


    public function getMachines(Request $request)
    {
       return Admin::form(ItemsCate::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->select('parent_id', trans('admin.parent_id'))->options(ItemsCate::selectOptions());
            $form->text('title', trans('admin.title'))->rules('required')->help('Required');
            $form->image('cate_img', "Icon")->uniqueName();
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
