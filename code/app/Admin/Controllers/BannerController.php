<?php

namespace App\Admin\Controllers;

use App\Models\Banner;

use App\Models\Items;
use App\Models\ItemsCate;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class BannerController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('轮播');
            $content->description('列表');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('首页轮播');
            $content->description('编辑');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('首页轮播');
            $content->description('创建');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Banner::class, function (Grid $grid) {
            $grid->model()->orderBy('sort', 'DESC');
            $grid->model()->orderBy('id', 'DESC');

            $grid->id('ID')->sortable();
            $grid->title('banner标题');
            $grid->images('banner图片')->display(function ($images) {
                return sprintf(
                    '<img  src="%s" class="img" />',
                    imageUrl($images, "resize,h_40")
                );
            });
            $grid->url("跳转商品链接")->display(function ($url) {
                $jump_link = empty($url) ? "javascript:;" : "";
                $jump_link_text = empty($url) ? "无跳转" : $this->title;
                $target = empty($url) ? "" : "target='_blank'";
                return "<a href='{$jump_link}' $target >$jump_link_text</a>";
            });

            $grid->filter(function ($filter) {
                $filter->like('title',"banner标题");
            });

            $grid->actions(function ($actions) {
                $actions->disableView();
            });

            /*$grid->tools(function ($tools) {
                $tools->batch(function ($batch) {
                    $batch->disableView();
                });
            });*/

            $grid->created_at("创建时间");
            $grid->updated_at("更新时间");
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Banner::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('title', "banner标题");
            $form->image('images', "banner图片")->rules('required')->uniqueName();
            $form->text('url', "商品页跳转")->default(0)->help("填写商品id，0是不跳转");
            $form->select('position','轮播位置')->options(ItemsCate::where('is_recommend',1)->pluck("title",'id')->prepend('精选', 0));
            $form->number('sort', "排序")->rules('numeric');


            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
