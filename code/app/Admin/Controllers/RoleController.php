<?php

namespace App\Admin\Controllers;

use App\Models\Permission;
use App\Models\Role;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Role);
        $grid->model()->with(['permissions']);

        $grid->id('Id');
        $grid->name('名称');
        $grid->slug('标识');
        $grid->permissions('权限')->pluck('name')->label('info');
        $grid->description('描述');
        $grid->addTime('添加时间')->display(function () {
            return $this->addTime ? $this->addTime->format('Y-m-d H:i:s') : '';
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Role::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->slug('Slug');
        $show->description('Description');
        $show->addTime('AddTime');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $model = new Role();
        $form = new Form($model);
        $table = $model->getTable();

        $form->text('name', '名称')->rules(['required']);
        $form->text('slug', '别名')->rules([
            'required', Rule::unique($table)->ignore(request()->route('role'))
        ])->help('创建后不能修改');
        $form->textarea('description', '描述');
        $form->checkbox('permissions', '分配权限')->options(Permission::pluck('name', 'id'));
        $form->saving(function ($form) {
            if ($form->model()->slug) {
                $form->slug = $form->model()->slug;
            }
        });
        return $form;
    }
}
