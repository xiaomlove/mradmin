<?php

namespace App\Admin\Controllers;

use App\Models\CouponCard;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AgentCouponCardController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('代理')
            ->description('充值卡管理列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('代理')
            ->description('充值卡详情')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('代理')
            ->description('充值卡编辑')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('代理')
            ->description('充值卡创建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CouponCard);

        $grid->id('Id');
        $grid->coupon_card_no('Coupon card no');
        $grid->coupon_card_pub_key('Coupon card pub key');
        $grid->coupon_card_pri_key('Coupon card pri key');
        $grid->first_class_agent_id('First class agent id');
        $grid->second_class_agent_id('Second class agent id');
        $grid->points_quota('Points quota');
        $grid->is_used('Is used');
        $grid->bind_uid('Bind uid');
        $grid->used_at('Used at');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        $grid->disableCreateButton();
        $grid->disableActions();

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CouponCard::findOrFail($id));

        $show->id('Id');
        $show->coupon_card_no('Coupon card no');
        $show->coupon_card_pub_key('Coupon card pub key');
        $show->coupon_card_pri_key('Coupon card pri key');
        $show->first_class_agent_id('First class agent id');
        $show->second_class_agent_id('Second class agent id');
        $show->points_quota('Points quota');
        $show->is_used('Is used');
        $show->bind_uid('Bind uid');
        $show->used_at('Used at');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CouponCard);

        $form->text('coupon_card_no', 'Coupon card no');
        $form->text('coupon_card_pub_key', 'Coupon card pub key');
        $form->text('coupon_card_pri_key', 'Coupon card pri key');
        $form->number('first_class_agent_id', 'First class agent id');
        $form->number('second_class_agent_id', 'Second class agent id');
        $form->number('points_quota', 'Points quota');
        $form->switch('is_used', 'Is used');
        $form->number('bind_uid', 'Bind uid');
        $form->datetime('used_at', 'Used at')->default(date('Y-m-d H:i:s'));

        return $form;
    }
}
