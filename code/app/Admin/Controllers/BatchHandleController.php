<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;


class BatchHandleController extends Controller
{
    use ModelForm;

    /**
     * @desc 批量操作
     * @param Request $request
     */
    public function batchHandle(Request $request)
    {
        if(empty($request->get('ids'))) return normalize("未选中目标！");
        $model = "App\\Models\\".$request->model_name;

        foreach ($model::find($request->get('ids')) as $model)
        {
            if($request->is_delete) $model->delete();
            else
            {
                $request_set_field = $request->get('set_field');
                $model->$request_set_field = $request->get('set_value');
            }

        }
        return api(0,"操作成功！",[]);
    }
}
