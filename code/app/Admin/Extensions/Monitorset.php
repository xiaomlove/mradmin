<?php
/**
 * Created by PhpStorm.
 * User: plusev
 * Date: 2018/3/19
 * Time: 16:14
 */
// namespace App\Admin\Extensions;
// use Encore\Admin\Admin;

namespace App\Admin\Extensions;
use Encore\Admin\Admin;
/**
 * @desc 监控设置
 * Class Monitorset
 * @package App\Admin\Extensions
 */
class Monitorset
{
    protected $id;
    protected $isMonitor;

    public function __construct($id,$isMonitor)
    {
        $this->id = $id;
        $this->isMonitor = $isMonitor;
    }

    protected function script()
    {
        $post_url = url("admin/productline/monitorSet");
        return <<<SCRIPT
$('.monitorsetbtn').on('click', function() {
    var id = $(this).data('id');
    var csrf_token = $(this).data('csrf');
    var ismonitor = $(this).data('ismonitor');
    $.ajax({
        method: 'post',
        url: '$post_url',
        data: {
            id:id,
            ismonitor:ismonitor
        },
        headers: {
            'X-CSRF-TOKEN': csrf_token
        },
        success: function (data) {
            $.pjax.reload('#pjax-container');
            if(data.ret == 0) toastr.success(data.msg);
            else toastr.error(data.msg);
        }
    });
});

SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());
        $csrf_token = csrf_token();
        if ($this->isMonitor == 0) {
            $monitorName = '启动监控';
        }else{
            $monitorName = '停止监控';
        }
        $appedhtml = <<<EOT
            |<a data-id="{$this->id}" data-csrf="$csrf_token" data-ismonitor="$this->isMonitor" class="btn btn-xs monitorsetbtn stopmonitor">{$monitorName}</a>
EOT;
        return $appedhtml;
    }

    public function __toString()
    {
        return $this->render();
    }
}