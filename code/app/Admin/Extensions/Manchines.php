<?php
// namespace App\Admin\Extensions;
// use Encore\Admin\Admin;
namespace App\Admin\Extensions;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Request;

class Manchines extends AbstractTool
{
    protected $productName;
    protected $manchines;
    protected $createUrl;


    public function __construct($productName = null,$manchines = null,$createUrl = null)
    {
        $this->productName = $productName;
        $this->manchines = $manchines;
        $this->createUrl = $createUrl;
    }


    protected function script()
    {
        $url = Request::fullUrlWithQuery(['mId' => '_mId_']);

        return <<<EOT
$(".manchinesradio").change(function(){
    var opt=$(".manchinesradio").val();
    var url = "$url".replace('_mId_', opt);
    
    $.pjax({container:'#pjax-container', url: url });
    $(".manchinesradio").find("option[value='2']").attr("selected",true);

});


EOT;
    }

    public function render()
    {
        Admin::script($this->script());

        $options = $this->manchines;

        $productName = $this->productName;
        $createUrl = $this->createUrl;

        return view('manchines', compact('createUrl','productName','options'));
    }
}