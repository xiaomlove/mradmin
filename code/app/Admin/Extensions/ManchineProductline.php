<?php
// namespace App\Admin\Extensions;
// use Encore\Admin\Admin;
namespace App\Admin\Extensions;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Request;

class ManchineProductline extends AbstractTool
{
    protected $productlines;

    public function __construct($productlines = null)
    {
        $this->productlines = $productlines;
    }


    protected function script()
    {
        $url = Request::fullUrlWithQuery(['productline' => '_productline_']);

        return <<<EOT

$('input:radio.manchine-productline').change(function () {
    var url = "$url".replace('_productline_', $(this).val());
    
    $.pjax({container:'#pjax-container', url: url });
    // $("input.manchine-productline[type='radio'][value='1']").attr("checked");

});

EOT;
    }

    public function render()
    {
        Admin::script($this->script());

        $options = $this->productlines;

        return view('productline', compact('options'));
    }
}