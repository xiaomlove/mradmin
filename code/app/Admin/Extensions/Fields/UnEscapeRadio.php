<?php

namespace App\Admin\Extensions\Fields;

use Encore\Admin\Form\Field\Radio;

class UnEscapeRadio extends Radio
{
    protected $view = 'un-escape-radio';
}