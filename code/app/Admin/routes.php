<?php

use Illuminate\Routing\Router;

/**
 * 自带路由。有部分需要重写
 */
$attributes = [
    'prefix'     => config('admin.route.prefix'),
    'middleware' => config('admin.route.middleware'),
];

app('router')->group($attributes, function ($router) {

    /* @var \Illuminate\Routing\Router $router */
    $router->namespace('Encore\Admin\Controllers')->group(function ($router) {

        /* @var \Illuminate\Routing\Router $router */
//        $router->resource('auth/users', 'UserController');
        $router->resource('auth/roles', 'RoleController');
        $router->resource('auth/permissions', 'PermissionController');
        $router->resource('auth/menu', 'MenuController', ['except' => ['create']]);
        $router->resource('auth/logs', 'LogController', ['only' => ['index', 'destroy']]);
    });

    $authController = config('admin.auth.controller', AuthController::class);

    /* @var \Illuminate\Routing\Router $router */
    $router->get('auth/login', $authController.'@getLogin');
    $router->post('auth/login', $authController.'@postLogin');
    $router->get('auth/logout', $authController.'@getLogout');
    $router->get('auth/setting', $authController.'@getSetting');
    $router->put('auth/setting', $authController.'@putSetting');
});

/**
 * 自定义路由
 */
Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as' => 'admin.',
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->get('lgb', 'HomeController@lgb');
    $router->post('batchHandle/batchHandle', 'BatchHandleController@batchHandle')->name("batchhandle.batchHandle");
    $router->get('productline/design', 'ProductlineController@design');
    $router->get('productline/getMachineTools', 'ProductlineController@getMachineTools');
    $router->get('productline/getMachineConveyors', 'ProductlineController@getMachineConveyors');
    $router->post('productline/monitorSet', 'ProductlineController@monitorSet');
    $router->get('productline/saveContent', 'ProductlineController@saveContent');
    $router->get('productline/getGraphic', 'ProductlineController@getGraphic');
    $router->get('productline/getProductline', 'ProductlineController@getProductline');
    $router->get('productline/savesettings', 'ProductlineController@saveSettings');

    
    $router->get('signalpoint/getMachines', 'SignalpointController@getMachines');


    //批量操作
    $router->resource('productline', ProductlineController::class);
    $router->resource('machine', MachineController::class);
    $router->resource('signalpoint', SignalpointController::class);
    $router->resource('macfaultconfig', MacfaultConfigController::class);
    
    // $router->post('signalpoint/{productlineId?}/{machineId?}', 'SignalpointController@index');
    
    // $router->post('pdlRunState/monitorSet', 'PdlRunStateController@monitorSet')->name("pdlRunState.monitorSet");
    // $router->get('productline/productlines', 'ProductlineController@index');
/*
    $router->get('tool', 'ToolController@index');
    $router->post('tool/token', 'ToolController@token');
    $router->resource('user', 'UserController');
    $router->get('user/select/json', 'UserController@selectJson');
    $router->resource('topic', 'TopicController');
    $router->resource('config', 'ConfigController');
    $router->resource('score', 'ScoreController');
    $router->resource('lucky-draw', 'LuckyDrawController');
    $router->resource('lucky-draw-prize', 'LuckyDrawPrizeController');
    $router->resource('lucky-draw-win-log', 'LuckyDrawWinLogController');
*/
    $router->resource('auth/users', 'AdminController');
    $router->resource('role', 'RoleController');
    $router->resource('permission', 'PermissionController');
    $router->resource('user', 'UserController');
    $router->resource('group-of-user', 'UserGroupController');
    $router->resource('member-of-user-group', 'UserGroupMemberController');
    $router->get('members/edit', 'UserGroupMemberController@editMembersForm')->name('group.editMembersForm');
    $router->post('members/edit', 'UserGroupMemberController@editMembersSave')->name('group.editMembersSave');
    $router->resource('type-of-product', 'ProductTypeController');
    $router->resource('palletizer-capacity', 'PalletizerCapacityController');
    $router->resource('stop-reason', 'StopReasonController');
    $router->resource('fault-desc', 'FaultDescController');
    $router->resource('alarm-monitor-productline', 'AlarmMonitorController');
    $router->resource('alarm-monitor-machine', 'AlarmMonitorController');
    $router->resource('alarm-monitor-signal-point', 'AlarmMonitorController');
    $router->resource('alarm-monitor-level', 'AlarmLevelController');
    $router->resource('affect-factor', 'AffectFactorController');

});
