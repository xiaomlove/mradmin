<?php

namespace App\Repositories;

use App\User;
use App\Models\SignInLog;
use App\Models\Score;

class UserRepository
{
    public function signIn(User $user)
    {
        $result = \DB::transaction(function () use ($user) {
            //插入签到记录
            $signInLog = $user->signInLogs()->create();
            //送积分
            $score = Score::assignForSignIn($user->id);
            return $signInLog;
        });

        return $result;
    }
}