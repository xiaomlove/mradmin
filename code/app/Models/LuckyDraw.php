<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LuckyDraw extends Model
{
    protected $fillable = ['title', 'description', 'cover', 'consume_score', 'begin_time', 'end_time'];

    public function prizes()
    {
        return $this->hasMany(LuckyDrawPrize::class, "lucky_draw_id");
    }

    public function winLogs()
    {
        return $this->hasMany(LuckyDrawWinLog::class, "lucky_draw_id");
    }

}
