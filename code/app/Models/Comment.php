<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Comment extends Model
{
    use SoftDeletes;

    const TARGET_TYPE_TOPIC = 1;

    protected $fillable = ['uid', 'target_type', 'target_id', 'pid', 'floor_num', 'content', 'images'];

    public function topic()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class, "uid");
    }

    public function likes()
    {
        return $this->morphMany(Like::class, "target");
    }

    public function applyJob()
    {
        return $this->hasOne(ApplyJob::class, "cid");
    }
}
