<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    const MACHINE_KEYTYPE_OTHERS = 0;
    const MACHINE_KEYTYPE_DRINK = 1;
    const MACHINE_KEYTYPE_STACKERCRANE = 2;
    const MACHINE_KEYTYPE_MINEFBI = 3;
    const MACHINE_KEYTYPE_LABELLER = 4;
    const MACHINE_KEYTYPE_UNPILER = 5;
    const MACHINE_KEYTYPE_SIGNFBI = 6;
    const MACHINE_KEYTYPE_CKBOTTLE = 7;
    const MACHINE_KEYTYPE_BOTTLE = 8;
    const MACHINE_KEYTYPE_STERILIZATION = 9;
    const MACHINE_KEYTYPE_BOX = 10;

    const MACHINE_TYPE_TOOL = 0;
    const MACHINE_TYPE_CONVEYOR = 1;

    public static $keyMachineTypeNames = [
        self::MACHINE_KEYTYPE_OTHERS => '其他',
        self::MACHINE_KEYTYPE_DRINK => '灌酒机',
        self::MACHINE_KEYTYPE_STACKERCRANE => '码垛机',
        self::MACHINE_KEYTYPE_MINEFBI => '酒机FBI机',
        self::MACHINE_KEYTYPE_LABELLER => '贴标机',
        self::MACHINE_KEYTYPE_UNPILER => '卸垛机',
        self::MACHINE_KEYTYPE_SIGNFBI => '标记FBI机',
        self::MACHINE_KEYTYPE_CKBOTTLE => '验瓶机',
        self::MACHINE_KEYTYPE_BOTTLE => '洗瓶机',
        self::MACHINE_KEYTYPE_STERILIZATION => '杀菌机',
        self::MACHINE_KEYTYPE_BOX => '成箱机',
    ];

    public static $typeNames = [
        self::MACHINE_TYPE_TOOL => '作业机台',
        self::MACHINE_TYPE_CONVEYOR => '输送链道'
    ];
    
    protected $table = 'hawk_machine';

    protected $fillable = ['opcGroupName', 'code', 'graphic', 'name', 'sort', 'produceLineId', 'keyMachineType', 'type'];

    public $timestamps = false;

    public function scopeInProductLine($query, $id)
    {
        return $query->where('produceLineId', $id);
    }

    public function productline()
    {
        return $this->belongsTo(Productline::class, 'produceLineId');
    }

    public function getTypeName(){
        return self::$typeNames[$this->type] ? self::$typeNames[$this->type] : '';
    }

    public function getKeyMachineTypeName(){
        return self::$keyMachineTypeNames[$this->keyMachineType] ? self::$keyMachineTypeNames[$this->keyMachineType] : '';
    }
}