<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlarmLevel extends Model
{
    protected $table = 'hawk_alarmlevel';

    public $timestamps = false;

    public static $icons = [
        'orange.png',
        'blue.png',
        'red.png',
        'yellow.png',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'icon', 'description',
    ];
}
