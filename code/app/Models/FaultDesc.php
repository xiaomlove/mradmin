<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaultDesc extends Model
{
    protected $table = 'hawk_faultdesc';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'stopreasonId',
    ];

    public function stopReason()
    {
        return $this->belongsTo(StopReason::class, 'stopreasonId');
    }
}
