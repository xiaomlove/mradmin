<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factory extends Model
{
    protected $table = 'hawk_factory';

    protected $fillable = ['name', 'logo'];

    public $timestamps = false;

    public function productline()
    {
        return $this->hasMany(Productline::class, 'factoryId');
    }
}
