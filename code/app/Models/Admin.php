<?php

namespace App\Models;

use Encore\Admin\Auth\Database\Administrator;
use App\User;

class Admin extends Administrator
{
    public static $basePermissions = ['auth.login', 'auth.setting'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            //基本的登录退出，查看个人信息权限
            $model->permissions()->saveMany(AdminPermission::whereIn('slug', self::$basePermissions)->get());
        });
    }

}
