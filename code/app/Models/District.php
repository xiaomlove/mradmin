<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = ['name', 'parent_id', 'code', 'order'];

    public function cities()
    {
        return $this->hasMany(__CLASS__, 'parent_id');
    }

    public function districts()
    {
        return $this->hasMany(__CLASS__, 'parent_id');
    }
}
