<?php

namespace App\Models;

use Encore\Admin\Auth\Database\Permission as LaravelAdminPermission;

class AdminPermission extends LaravelAdminPermission
{
    const SLUG_USER_MANAGEMENT = 'user_management';
    const SLUG_PRODUCT_TYPE_MANAGEMENT = 'product_type_management';
    const SLUG_USER_GROUP_MANAGEMENT = 'user_group_management';
    const SLUG_STOP_REASON_MANAGEMENT = 'stop_reason_management';
    const SLUG_PRODUCT_LINE_MANAGEMENT = 'product_line_management';
    const SLUG_ALARM_MONITOR_MANAGEMENT = 'alarm_monitor_management';
    const SLUG_PRODUCT_LINE_AFFECT_FACTOR_MANAGEMENT = 'product_line_affect_factor_management';
    const SLUG_USER_ROLE_MANAGEMENT = 'user_role_management';
    const SLUG_CONFIG_MANAGEMENT = 'config_management';

    public static $defaults = [
        self::SLUG_USER_MANAGEMENT => ['name' => '员工管理', 'http_path' => '/user*'],
        self::SLUG_PRODUCT_TYPE_MANAGEMENT => ['name' => '品种管理', 'http_path' => '/product-type*'],
        self::SLUG_USER_GROUP_MANAGEMENT => ['name' => '班组管理', 'http_path' => '/group-of-user*'],
        self::SLUG_STOP_REASON_MANAGEMENT => ['name' => '停机原因管理', 'http_path' => '/stop-reason*'],
        self::SLUG_PRODUCT_LINE_MANAGEMENT => ['name' => '产生线管理', 'http_path' => '/productline*'],
        self::SLUG_ALARM_MONITOR_MANAGEMENT => ['name' => '报警监控管理', 'http_path' => '/alarm-monitor*'],
        self::SLUG_PRODUCT_LINE_AFFECT_FACTOR_MANAGEMENT => ['name' => '三大指标配置', 'http_path' => '/affect-factor*'],
        self::SLUG_USER_ROLE_MANAGEMENT => ['name' => '员工角色管理', 'http_path' => '/role*'],
        self::SLUG_CONFIG_MANAGEMENT => ['name' => '系统设置', 'http_path' => '/config*'],
    ];

    public static function selectOptions()
    {
        return static::whereNotIn('slug', Admin::$basePermissions)->pluck('name', 'id');
    }

}
