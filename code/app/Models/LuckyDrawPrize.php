<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LuckyDrawPrize extends Model
{
    protected $fillable = ['lucky_draw_id', 'agent_id', 'title', 'cover', 'description', 'possibility', 'award_one_day', 'award_total'];

    public function luckyDraw()
    {
        return $this->belongsTo(LuckyDraw::class, 'lucky_draw_id');
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class, 'agent_id');
    }

    public function winLogs()
    {
        return $this->hasMany(LuckyDrawWinLog::class, 'lucky_draw_prize_id');
    }
}
