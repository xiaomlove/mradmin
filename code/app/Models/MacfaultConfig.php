<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MacfaultConfig extends Model
{

	const SIGNALPOINT_VALUETYPE_LIMIT = 1;
    const SIGNALPOINT_VALUETYPE_THRESHOLD = 2;

    const SIGNALPOINT_FAULTTYPE_DEFAULT = 0;
    const SIGNALPOINT_FAULTTYPE_CRASH = 1;
    const SIGNALPOINT_FAULTTYPE_STATE = 2;
    const SIGNALPOINT_FAULTTYPE_FAULT = 3;

    public static $valueTypeNames = [
        self::SIGNALPOINT_VALUETYPE_LIMIT => '限位值',
        self::SIGNALPOINT_VALUETYPE_THRESHOLD => '阈值'
    ];

    public static $faultTypeNames = [
        self::SIGNALPOINT_FAULTTYPE_DEFAULT => '未知',
        self::SIGNALPOINT_FAULTTYPE_CRASH => '停机产生的故障',
        self::SIGNALPOINT_FAULTTYPE_STATE => '状态点位产生的故障',
        self::SIGNALPOINT_FAULTTYPE_FAULT => '故障点位产生的故障',
    ];
    
    protected $table = 'hawk_macfaultconfig';

    protected $fillable = ['machineId', 'description', 'name', 'valueType', 'maximum', 'minimum', 'threshold', 'graphic', 'color', 'signalPointId', 'faultType'];

    public $timestamps = false;

    public function machine()
    {
        return $this->belongsTo(Machine::class, 'machineId');
    }

    public function signalpoint()
    {
        return $this->belongsTo(Signalpoint::class, 'signalPointId');
    }

    public function getValueTypeName()
    {
        return self::$valueTypeNames[$this->valueType] ? self::$valueTypeNames[$this->valueType] : '';
    }

    public function isLimitValue()
    {
        return $this->valueType == self::SIGNALPOINT_VALUETYPE_LIMIT ? true : false;
    }

    public static function getFaultType($signalPointType)
    {
        if ($signalPointType == Signalpoint::SIGNALPOINT_TYPE_STARTSTOP) {
            return self::SIGNALPOINT_FAULTTYPE_CRASH;
        }else if ($signalPointType == Signalpoint::SIGNALPOINT_TYPE_STATUS) {
            return self::SIGNALPOINT_FAULTTYPE_STATE;
        }else if ($signalPointType == Signalpoint::SIGNALPOINT_TYPE_FAULT) {
            return self::SIGNALPOINT_FAULTTYPE_FAULT;
        }else{
            return self::SIGNALPOINT_FAULTTYPE_DEFAULT;
        }
    }


}