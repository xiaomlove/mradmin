<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffectFactor extends Model
{
    protected $table = 'hawk_pdlindaffectfactor';

    public $timestamps = false;

    protected $fillable = [
        'productlineId', 'name', 'affectType', 'stopreasonId', 'pindex',
    ];

    const AFFECT_TYPE_STOP_REASON = 1;
    const AFFECT_TYPE_STAT_INDEX = 2;

    public static $affectTypeNames = [
        self::AFFECT_TYPE_STOP_REASON => '停机原因',
        self::AFFECT_TYPE_STAT_INDEX => '统计指标',
    ];

    public function productline()
    {
        return $this->belongsTo(Productline::class, 'productlineId');
    }

    public function stopReason()
    {
        return $this->belongsTo(StopReason::class, 'stopreasonId');
    }

    public function getAffectTypeNameAttribute()
    {
        return self::$affectTypeNames[$this->affectType] ?? '';
    }

    public function getPindexNameAttribute()
    {
        return AlarmMonitor::$indexNames[$this->pindex] ?? '';
    }
}
