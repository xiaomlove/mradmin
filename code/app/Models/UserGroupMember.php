<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserGroupMember extends Model
{
    protected $table = 'hawk_groupmember';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId', 'groupId', 'isLeader', 'opermachineId',
    ];

    public function getIsLeaderTextAttribute()
    {
        return $this->isLeader ? '是' : '否';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'userId');
    }

    public function operateMachine()
    {
        return $this->belongsTo(Machine::class, 'opermachineId');
    }


}
