<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductLineMember extends Pivot
{
    protected $table = 'hawk_pdlmember';

    protected $fillable = ['productlineId', 'userId', 'addTime', 'updateTime'];

    const CREATED_AT = 'addTime';
    const UPDATED_AT = 'updateTime';

    protected $dateFormat = 'U';
}
