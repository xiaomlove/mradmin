<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Signalpoint extends Model
{

	const SIGNALPOINT_VALTYPE_SWICHKEY = 0;
    const SIGNALPOINT_VALTYPE_ANALOG = 1;

	const SIGNALPOINT_TYPE_STARTSTOP = 0;
    const SIGNALPOINT_TYPE_STATUS = 1;
    const SIGNALPOINT_TYPE_SPEED = 2;
    const SIGNALPOINT_TYPE_OUTPUT = 3;
    const SIGNALPOINT_TYPE_FAULT = 4;
    const SIGNALPOINT_TYPE_REMOVE = 5;
    const SIGNALPOINT_TYPE_OTHERS = 6;

    const SIGNALPOINT_SAMPLETYPE_N = 0;
    const SIGNALPOINT_SAMPLETYPE_Y = 1;

    public static $typeNames = [
        
        self::SIGNALPOINT_TYPE_STARTSTOP => '启停点位',
        self::SIGNALPOINT_TYPE_STATUS => '状态点位',
        self::SIGNALPOINT_TYPE_SPEED => '速度点位',
        self::SIGNALPOINT_TYPE_OUTPUT => '产量点位',
        self::SIGNALPOINT_TYPE_FAULT => '故障点位',
        self::SIGNALPOINT_TYPE_REMOVE => '剔除量点位',//FBI机才会出现
        self::SIGNALPOINT_TYPE_OTHERS => '其他',
    ];

    public static $valTypeNames = [
        self::SIGNALPOINT_VALTYPE_SWICHKEY => '开关量',
        self::SIGNALPOINT_VALTYPE_ANALOG => '模拟量'
    ];

    public static $sampleTypeNames = [
        self::SIGNALPOINT_SAMPLETYPE_N => '不采样',
        self::SIGNALPOINT_SAMPLETYPE_Y => '采样'
    ];
    
    protected $table = 'hawk_signalpoint';

    protected $fillable = ['opcItemName', 'unit', 'name', 'defaultValue', 'type', 'valType', 'productlineId', 'machineId', 'isSample', 'sampleRate'];

    public $timestamps = false;

    public function productline()
    {
        return $this->belongsTo(Productline::class, 'produceLineId');
    }

    public function machine()
    {
        return $this->belongsTo(Machine::class, 'machineId');
    }

    public function getTypeName()
    {
        return self::$typeNames[$this->type] ? self::$typeNames[$this->type] : '';
    }

    public function getValTypeName()
    {
        return self::$valTypeNames[$this->valType] ? self::$valTypeNames[$this->valType] : '';
    }

    public function getSampleTypeName()
    {
        return self::$sampleTypeNames[$this->isSample] ? self::$sampleTypeNames[$this->isSample] : '';
    }

    public static function selectOptions($productLineId)
    {
        $out = [];

        static::where('productlineId', $productLineId)->with('machine')->orderBy('machineId', 'asc')->get()
            ->each(function ($item) use (&$out) {
                $out[$item->id] = sprintf('%s(机台：%s)', $item->name, $item->machine->name);
            });

        return $out;
    }

}
