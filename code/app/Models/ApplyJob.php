<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplyJob extends Model
{
    use HasDistrict;

    protected $fillable = ['uid', 'tid', 'province_code', 'city_code', 'district_code', 'phone', 'intention'];

    public function comment()
    {
        return $this->belongsTo(Comment::class, "cid");
    }
}
