<?php

namespace App\Models;

use Encore\Admin\Auth\Database\Menu as AdminMenu;

class Menu extends AdminMenu
{
    /**
     * 默认菜单
     * @var array
     */
    public static $defaults = [
        [
            'title' => '生产线管理',
            'icon' => 'fa-server',
            'uri' => 'productline',
            'roles' => [],
            'children' => [
                [
                    'title' => '生产线',
                    'icon' => 'fa-barcode',
                    'uri' => 'productline',
                    'roles' => [],
                ],
                [
                    'title' => '机台',
                    'icon' => 'fa-archive',
                    'uri' => 'machine',
                    'roles' => [],
                ]
            ]
        ],
        [
            'title' => '品种管理',
            'icon' => 'fa-user-plus',
            'uri' => 'type-of-product',
            'roles' => [],
        ],
        [
            'title' => '品种配置',
            'icon' => 'fa-user-plus',
            'uri' => 'palletizer-capacity',
            'roles' => [],
        ],
        [
            'title' => '停机原因',
            'icon' => 'fa-user-plus',
            'uri' => 'stop-reason',
            'roles' => [],
        ],
        [
            'title' => '报警监控',
            'icon' => 'fa-user-plus',
            'uri' => 'alarm-monitor',
            'roles' => [],
            'children' => [
                [
                    'title' => '生产线',
                    'icon' => 'fa-barcode',
                    'uri' => 'alarm-monitor-productline',
                    'roles' => [],
                ],
                [
                    'title' => '机台',
                    'icon' => 'fa-archive',
                    'uri' => 'alarm-monitor-machine',
                    'roles' => [],
                ],
                [
                    'title' => '点位',
                    'icon' => 'fa-archive',
                    'uri' => 'alarm-monitor-signal-point',
                    'roles' => [],
                ],
                [
                    'title' => '报警等级',
                    'icon' => 'fa-archive',
                    'uri' => 'alarm-monitor-level',
                    'roles' => [],
                ]
            ]
        ],
        [
            'title' => '停机原因',
            'icon' => 'fa-user-plus',
            'uri' => 'stop-reason',
            'roles' => [],
        ],
        [
            'title' => '三大指标影响因子',
            'icon' => 'fa-user-plus',
            'uri' => 'affect-factor',
            'roles' => [],
        ],
        [
            'title' => '员工',
            'icon' => 'fa-user-plus',
            'uri' => 'user',
            'roles' => [],
        ],
        [
            'title' => '班组',
            'icon' => 'fa-user-plus',
            'uri' => 'group-of-user',
            'roles' => [],
        ],
        [
            'title' => '角色管理',
            'icon' => 'fa-user-plus',
            'uri' => 'role',
            'roles' => [],
        ],
        [
            'title' => '权限管理',
            'icon' => 'fa-user-plus',
            'uri' => 'permission',
            'roles' => [],
        ],
    ];
}
