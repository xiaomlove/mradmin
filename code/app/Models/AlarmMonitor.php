<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlarmMonitor extends Model
{
    protected $table = 'hawk_alarmmonitor';

    public $timestamps = false;

    const TARGET_TYPE_PRODUCT_LINE = 1;
    const TARGET_TYPE_MACHINE = 2;
    const TARGET_TYPE_SIGNAL_POINT = 3;

    public static $targetTypeNames = [
        self::TARGET_TYPE_PRODUCT_LINE => '生产线',
        self::TARGET_TYPE_MACHINE => '机台',
        self::TARGET_TYPE_SIGNAL_POINT => '信号点位',
    ];

    const INDEX_GLY = 1;
    const INDEX_OEE = 2;
    const INDEX_LEF = 3;
    const INDEX_BOTTLE_OUTPUT = 4;
    const INDEX_BOTTLE_LOSS = 5;
    const INDEX_KICK_OUT_AMOUNT = 6;
    const INDEX_LOSS_TIME = 7;

    const INDEX_REAL_TIME_SPEED = 8;
    const INDEX_REAL_TIME_OUTPUT = 9;
    const INDEX_MACHINE_STOP_TIMES = 10;

    public static $indexNames = [
        self::INDEX_GLY => 'GLY',
        self::INDEX_OEE => 'OEE',
        self::INDEX_LEF => 'LEF',
        self::INDEX_BOTTLE_OUTPUT => '瓶产量',
        self::INDEX_BOTTLE_LOSS => '瓶损失',
        self::INDEX_KICK_OUT_AMOUNT => '踢除数',
        self::INDEX_LOSS_TIME => '损失时间',

        self::INDEX_REAL_TIME_SPEED => '实时速度',
        self::INDEX_REAL_TIME_OUTPUT => '实时产量',
        self::INDEX_MACHINE_STOP_TIMES => '停机次数',
    ];

    public static $indexGroups = [
        self::TARGET_TYPE_PRODUCT_LINE => [
            self::INDEX_GLY,
            self::INDEX_OEE,
            self::INDEX_LEF,
            self::INDEX_BOTTLE_OUTPUT,
            self::INDEX_BOTTLE_LOSS,
            self::INDEX_KICK_OUT_AMOUNT,
            self::INDEX_LOSS_TIME,
        ],
        self::TARGET_TYPE_MACHINE => [
            self::INDEX_REAL_TIME_SPEED,
            self::INDEX_REAL_TIME_OUTPUT,
            self::INDEX_MACHINE_STOP_TIMES,
        ],
    ];

    const TIME_RANGE_REAL_TIME = 1;
    const TIME_RANGE_FROM_ZERO_O_CLOCK_TO_CURRENT = 2;
    const TIME_RANGE_FROM_CHANGE_SHIFT_TO_CURRENT = 3;
    const TIME_RANGE_FROM_PUT_INTO_USE_TO_CURRENT = 4;

    public static $timeRangeNames = [
        self::TIME_RANGE_REAL_TIME => '实时动态(2小时以内)',
        self::TIME_RANGE_FROM_ZERO_O_CLOCK_TO_CURRENT => '零点至今',
        self::TIME_RANGE_FROM_CHANGE_SHIFT_TO_CURRENT => '换班至今',
        self::TIME_RANGE_FROM_PUT_INTO_USE_TO_CURRENT => '开线至今',
    ];

    const VALUE_TYPE_THRESHOLD = 1;
    const VALUE_TYPE_RANGE = 2;

    public static $valueTypeNames = [
        self::VALUE_TYPE_THRESHOLD => '阀值',
        self::VALUE_TYPE_RANGE => '限位值',
    ];

    public static function indexOfProductLine()
    {
        return array_only(self::$indexNames, self::$indexGroups[self::TARGET_TYPE_PRODUCT_LINE]);
    }

    public static function indexOfMachine()
    {
        return array_only(self::$indexNames, self::$indexGroups[self::TARGET_TYPE_MACHINE]);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'productlineId', 'alarmTip', 'targetName', 'targetId', 'targetType', 'valueType',
        'maximum', 'minimum', 'threshold', 'alarmLevelId', 'pindex', 'monitorTime'
    ];


    public function getTargetTypeNameAttribute()
    {
        return self::$targetTypeNames[$this->targetType] ?? '';
    }

    public function getIndexNameAttribute()
    {
        return self::$indexNames[$this->pindex] ?? '';
    }

    public function getTimeRangeNameAttribute()
    {
        return self::$timeRangeNames[$this->monitorTime] ?? '';
    }

    public function getValueTypeNameAttribute()
    {
        return self::$valueTypeNames[$this->valueType] ?? '';
    }

    public function target()
    {
        return $this->morphTo('target', 'targetType', 'targetId');
    }

    public function productline()
    {
        return $this->belongsTo(Productline::class, 'productlineId');
    }

    public function level()
    {
        return $this->belongsTo(AlarmLevel::class, 'alarmLevelId');
    }
}
