<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Score extends Model
{
    const TYPE_OTHER = 0;
    const TYPE_REGISTER = 1;
    const TYPE_SHARE_ITEM = 2;
    const TYPE_SHARE_TOPIC = 3;
    const TYPE_LUCKY_DRAW_AWARD = 4;
    const TYPE_SIGN_IN = 5;
    const TYPE_COUPON_CARD_TOP_UP = 6;

    public static $typeTexts = [
        self::TYPE_OTHER =>['name'=> '其他', 'value' => 10], //一般是特殊临时性质
        self::TYPE_REGISTER =>['name'=>  '新用户注册', 'value' => 10],
        self::TYPE_SHARE_ITEM =>['name'=>  '分享商品', 'value' => 10],
        self::TYPE_SHARE_TOPIC =>['name'=>  '分享话题', 'value' => 10],
        self::TYPE_LUCKY_DRAW_AWARD =>['name'=> '进行抽奖', 'value' => -10],
        self::TYPE_SIGN_IN =>['name'=> '签到奖励', 'value' => 10],
        self::TYPE_COUPON_CARD_TOP_UP =>['name'=> '充值卡充值', 'value' => 0],
    ];

    protected $fillable = ['uid', 'type', 'score', 'total', 'remarks', 'info'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
           $total = $model->where("uid", $model->uid)->sum('score');
           $model->total = $total;
           $model->save();
           User::where('id', $model->uid)->update(['scores' => $total]);
        });
    }

    public function getTypeTextAttribute()
    {
        return self::$typeTexts[$this->type] ? self::$typeTexts[$this->type]['name'] : '';
    }

    public function user()
    {
        return $this->belongsTo(User::class, "uid");
    }

    /**
     * 其他类型的积分发送，一般是不固定临时或者活动性质的
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     */
    public static function assignForOther($uid, $score, $remarks, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_OTHER,
            'score' => $score,
            'remarks' => $remarks,
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 用户新注册时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     */
    public static function assignForRegister($uid, $score = null, $remarks = null, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_REGISTER,
            'score' => $score ?? Config::getRegisterScore() ?? self::$typeTexts[self::TYPE_REGISTER]['value'],
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_REGISTER]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 分享商品时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     */
    public static function assignForShareItem($uid, $score = null, $remarks = null, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_SHARE_ITEM,
            'score' => $score ?? Config::getShareItemScore() ?? self::$typeTexts[self::TYPE_SHARE_ITEM]['value'],
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_SHARE_ITEM]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 分享帖子时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     *
     */
    public static function assignForShareTopic($uid, $score = null, $remarks = null, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_SHARE_TOPIC,
            'score' => $score ?? Config::getShareTopicScore() ?? self::$typeTexts[self::TYPE_SHARE_TOPIC]['value'],
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_SHARE_TOPIC]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 进行抽奖时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     *
     */
    public static function assignForLuckyDrawAward($uid, $score = null, $remarks = null, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_LUCKY_DRAW_AWARD,
            'score' => $score ?? self::$typeTexts[self::TYPE_LUCKY_DRAW_AWARD]['value'],
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_LUCKY_DRAW_AWARD]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 签到时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     *
     */
    public static function assignForSignIn($uid, $score = null, $remarks = null, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_SIGN_IN,
            'score' => $score ?? self::$typeTexts[self::TYPE_SIGN_IN]['value'],
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_SIGN_IN]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 充值卡充值积分时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     *
     */
    public static function assignForCouponCardTopUp($uid, $score = null, $remarks = null, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_COUPON_CARD_TOP_UP,
            'score' => $score ?? self::$typeTexts[self::TYPE_COUPON_CARD_TOP_UP]['value'],
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_COUPON_CARD_TOP_UP]['name'],
            'info' => json_encode($info, 336),
        ]);
    }
}
