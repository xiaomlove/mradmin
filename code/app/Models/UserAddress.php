<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserAddress extends Model
{
    use HasDistrict;

    protected $fillable = ['uid', 'province_code', 'city_code', 'district_code', 'address', 'phone', 'priority'];

    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }
}
