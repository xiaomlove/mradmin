<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsSendLog extends Model
{
    const SEND_RESULT_SUCCESS = 0;

    const SEND_RESULT_FAIL = 1;

    private static $sendResults = [
        self::SEND_RESULT_SUCCESS => '成功',
        self::SEND_RESULT_FAIL => '失败',
    ];

    protected $fillable = [
        'uid', 'phone', 'code', 'expires_at', 'used_at',
        'send_result_info', 'send_result',
    ];

    public function getSendResultTextAttribute()
    {
        if (isset(self::$sendResults[$this->send_result]))
        {
            self::$sendResults[$this->send_result];
        }
        return "";
    }

    public function scopeIsSuccess($query)
    {
        $query->where("send_result", self::SEND_RESULT_SUCCESS);
    }

}
