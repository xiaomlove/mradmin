<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PalletizerCapacity extends Model
{
    protected $table = 'hawk_producttypeconfig';

    public $timestamps = false;

    const UNIT_DUO = 1;
    const UNIT_BOX = 2;

    public static $unitTexts = [
        self::UNIT_DUO => '跺',
        self::UNIT_BOX => '箱',
    ];

    public function getUnitTextAttribute()
    {
        return self::$unitTexts[$this->unit] ?? '';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'productlineId', 'productTypeId', 'boxNumber', 'bottleNumber', 'unit', 'ratedOutput',
    ];

    public function productline()
    {
        return $this->belongsTo(Productline::class, 'productlineId');
    }

    public function productType()
    {
        return $this->belongsTo(ProductType::class, 'productTypeId');
    }

}
