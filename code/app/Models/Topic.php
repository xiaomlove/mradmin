<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{
    use SoftDeletes;

    const TYPE_RECRUITMENT = 1;
    const TYPE_FUNNY_MOMENT = 2;
    const TYPE_APPLY_JOB = 3;

    public static $typeNames = [
        self::TYPE_RECRUITMENT => '招聘',
        self::TYPE_FUNNY_MOMENT => '趣事',
        self::TYPE_APPLY_JOB => '求职',
    ];

    protected $fillable = ['uid', 'type'];

    public function comments()
    {
        return $this->morphMany(Comment::class, "target");
    }

    public function firstComment()
    {
        return $this->comments()->where("floor_num", 1);
    }

    public function user()
    {
        return $this->belongsTo(User::class, "uid");
    }

}
