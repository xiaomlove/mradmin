<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Productline extends Model
{

    protected $table = 'hawk_productline';

    protected $fillable = ['opcHostIP', 'opcServerName', 'graphic', 'name', 'shortDownTimes', 'longDownTimes', 'factoryId'];

    public $timestamps = false;

    public function machines()
    {
        return $this->hasMany(Machine::class, 'produceLineId');
    }

    public function signalpoints()
    {
        return $this->hasMany(Signalpoint::class, 'productlineId');
    }

    public function pdlRunState()
    {
        return $this->hasOne(PdlRunState::class, 'productlineId');
    }

    public function factory()
    {
        return $this->belongsTo(Factory::class, 'factoryId');
    }

    public function alarmMonitor()
    {
        return $this->morphMany(AlarmMonitor::class, 'target', 'targetType', 'targetId');
    }

    public function affectFactors()
    {
        return $this->hasMany(AffectFactor::class, 'productlineId');
    }
}