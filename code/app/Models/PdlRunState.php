<?php
//生产线 运行状态
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PdlRunState extends Model
{

	const PDLRUNSTATE_MONITORTYPE_NOMONITOR = 0;
    const PDLRUNSTATE_MONITORTYPE_MONITORING = 1;

	const PDLRUNSTATE_MANUFACTURETYPE_NOMANUFACTUR = 0;
    const PDLRUNSTATE_MANUFACTURETYPE_MANUFACTURING = 1;

    public static $monitorTypeNames = [
        self::PDLRUNSTATE_MONITORTYPE_NOMONITOR => '待监控',
        self::PDLRUNSTATE_MONITORTYPE_MONITORING => '监控中'
    ];

    public static $manufactureStateNames = [
        self::PDLRUNSTATE_MANUFACTURETYPE_NOMANUFACTUR => '无生产',
        self::PDLRUNSTATE_MANUFACTURETYPE_MANUFACTURING => '生产中'
    ];
    
    protected $table = 'hawk_pdlrunstate';

    protected $fillable = ['productlineId', 'startRunTime', 'isMonitor', 'state'];

    public $timestamps = false;

    public function productline()
    {
        return $this->belongsTo(Productline::class, 'productlineId');
    }

    public function getMonitorTypeName()
    {
        return self::$monitorTypeNames[$this->isMonitor] ? self::$monitorTypeNames[$this->isMonitor] : '';
    }

    public static function getDefaultMonitorTypeName()
    {
        return self::$monitorTypeNames[self::PDLRUNSTATE_MONITORTYPE_NOMONITOR];
    }

    public function isMonitoring(){
        return ($this->isMonitor == self::PDLRUNSTATE_MONITORTYPE_MONITORING) ? true : false;
    }
}
