<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    const NAME_REGISTER_SCORE = 'register_score';
    const NAME_SHARE_ITEM_SCORE = 'share_item_score';
    const NAME_SHARE_TOPIC_SCORE = 'share_topic_score';
    const NAME_ABOUT = 'about';

    protected $fillable = ['name', 'name_human', 'value'];

    public static $defaults = [
        self::NAME_REGISTER_SCORE => ['name'=> '注册赠送积分', 'value' => 10],
        self::NAME_SHARE_ITEM_SCORE => ['name'=> '分享商品积分','value' => 10],
        self::NAME_SHARE_TOPIC_SCORE => ['name'=> '分享话题积分','value' => 10],
        self::NAME_ABOUT => ['name'=> '关于','value' => '这是一款集...于一体的app'],
    ];

    /**
     * 获取新用户注册所赠送的积分
     *
     * @return mixed
     */
    public static function getRegisterScore()
    {
        return optional((new static())->where('name', self::NAME_REGISTER_SCORE)->first())->value;
    }

    public static function getShareItemScore()
    {
        return optional((new static())->where('name', self::NAME_SHARE_ITEM_SCORE)->first())->value;
    }

    public static function getShareTopicScore()
    {
        return optional((new static())->where('name', self::NAME_SHARE_TOPIC_SCORE)->first())->value;
    }

    public static function getAbout()
    {
        return optional((new static())->where('name', self::NAME_ABOUT)->first())->value;
    }
}
