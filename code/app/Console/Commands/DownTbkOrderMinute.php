<?php

namespace App\Console\Commands;

use App\Models\TbkOficalOrder;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class DownTbkOrderMinute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'downorder:minute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每分钟同步订单数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $before_twenty_time = urlencode(date('Y-m-d H:i:s',time() - 20 * 60));
        $before_twenty_time = '2018-11-16+14%3a10%3a00';
        $api_url = sprintf('http://apiorder.vephp.com/order?vekey=%s&start_time=%s',env('VETBK_KEY'),$before_twenty_time);
        $client = new Client();
        $res = $client->request('GET', $api_url,[
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'     => 'application/json',
            ]
        ]);
        $body = (string)$res->getBody();
        $json = \GuzzleHttp\json_decode($body,true);
        if(@$json['error'] == 0)
        {
            if($json['data'])
            {
                array_walk($json['data'], function (&$value, $key) {
                    $value['created_at'] = date('Y-m-d H:i:s');
                    $value['updated_at'] = date('Y-m-d H:i:s');
                });
                TbkOficalOrder::insert($json['data']);
                $this->info("excute success");
            }
        }
    }
}
