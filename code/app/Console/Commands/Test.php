<?php

namespace App\Console\Commands;

use App\Models\AlarmMonitor;
use App\Models\Productline;
use App\Models\Signalpoint;
use Illuminate\Console\Command;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lgb:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'lgb test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $r = Signalpoint::selectOptions(1);
        dd($r);
    }
}
