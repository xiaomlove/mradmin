<?php

namespace App\Console\Commands;

use App\Models\Items;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class CleanUpOverdueGoods extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vegoods:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'clean up overdue the goods';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $goods = Items::all()->toArray();
        if(!empty($goods))
        {
            foreach ($goods as $k=>$v)
            {
                if(strtotime($v['coupon_end_time']) <= time())
                {
                    //清理过期的优惠券商品
                    Items::where("id",$v['id'])->delete();
                    Redis::srem(env('ITEMS_CRAWLERED_COLLECTIONS_REDIS_KEY'), $v['id']);
                }
                usleep(5000);
            }
        }
    }
}
