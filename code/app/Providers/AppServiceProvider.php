<?php

namespace App\Providers;

use App\Models\AlarmMonitor;
use App\Models\Like;
use App\Models\Machine;
use App\Models\Productline;
use App\Models\Signalpoint;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Relations\Relation;

use App\Models\Comment;
use App\Models\Topic;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Illuminate\Support\Facades\Schema::defaultStringLength(191);
        Carbon::setLocale('zh');
        $this->customRule();
        $this->customMorphMay();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\Faker\Generator::class, function() {
            return \Faker\Factory::create('zh_CN');
        });
    }

    private function customRule()
    {
        \Validator::extend('phone', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^1[\d]{10}$/', $value);
        }, ':attribute 不是合法的手机号码');
    }

    private function customMorphMay()
    {
        Relation::morphMap([
            AlarmMonitor::TARGET_TYPE_PRODUCT_LINE => Productline::class,
            AlarmMonitor::TARGET_TYPE_MACHINE => Machine::class,
            AlarmMonitor::TARGET_TYPE_SIGNAL_POINT => Signalpoint::class,
        ]);
    }

}
