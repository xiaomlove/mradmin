@extends('admin::index', ['header' => $header])

@section('content')
    <section class="content-header">
        <h1>
            {!! $header ?: trans('admin.title') !!}
            <small>{{ $description ?: trans('admin.description') }}</small>
            <div class="pull-right">
                <div class="btn-group pull-right" style="margin-right: 50px">
                    <a href="javascript:void(0)" class="btn btn-sm btn-default settingsbtn" title="设置">
                        <i class="fa fa-save"></i><span class="hidden-xs">&nbsp;&nbsp;设置</span>
                    </a>
                    <a href="javascript:void(0)" class="btn btn-sm btn-default savebtn" title="保存">
                        <i class="fa fa-save"></i><span class="hidden-xs">&nbsp;&nbsp;保存</span>
                    </a>
                </div>
            </div>
        </h1>
        
    </section>

    <section class="content">
        <link rel="stylesheet" type="text/css" href="{{ admin_asset("/css/productlineEditor.css") }}{{'?r='.rand(0,99999)}}">
        <link rel="stylesheet" type="text/css" href="{{ admin_asset("/css/coneditor.css") }}{{'?r='.rand(0,99999)}}">
        <script src="{{ admin_asset("/js/machine.js") }}{{'?r='.rand(0,99999)}}"></script>
        <script src="{{ admin_asset("/js/contenteditor.js") }}{{'?r='.rand(0,99999)}}"></script>
        @include('admin::partials.alerts')
        @include('admin::partials.exception')
        @include('admin::partials.toastr')

        <!-- 内容编辑区  start -->
        <div id="page-canvas">
            <div id="scrollable-content" class="scrollable-content">
                <div class="canvas coordinate-box bottom-margin-buffer" id="iframe-wrap" tabindex="-1" data-step="1" data-position='left'>
                    <!-- <link rel="stylesheet" href="/zneditor/css/font.css" />
                    <link rel="stylesheet" type="text/css" href="/extensions/widget.css"> -->
                    <div class="concontainer" id="concontainer"  style="width: 720px; height: 410px; transform: none; margin-top: 85px; margin-left: 364px;">
                        <div id="content-wrap" class="bluetpl">
                        </div>
                    </div>
                    <!--<iframe style="width: 720px;height: 410px;display: none" id="content-preview-iframe" name="content-preview-iframe"></iframe>-->
                </div>
            </div>
        </div>
    <!-- 内容编辑区  end -->
    <!-- 元素选择区  end -->
    <div id="page-elements" data-step="2" data-position='left'>
      <ul class="nav nav-tabs nav-tabs-justified picker-header">
        <li class="picker-group active"> <a href="#tools-tab" data-toggle="tab" aria-expanded="true" style="border-right: 1px solid #eee;"><span>机台</span></a> </li>
        <li class="picker-group"> <a href="#conveyors-tab" data-toggle="tab" aria-expanded="true"><span>链道</span></a> </li>
      </ul>
      
      <div class="tab-content clearfix elem-catalog elem-snippets-wrap" style="padding:0;margin-top: 20px;">
        <!-- 机台选择 -->
        <div class="tab-pane active elem-snippets" id="tools-tab" style="height:100%;z-index:10">
            <div class="scroll-content widget-container">
                <div class="widget-content" id="machine_tools">
                    
                </div>
            </div>
        </div>

        <!-- 链道选择   -->
    <div class="tab-pane" id="conveyors-tab">
        <div class="panel-body" style="height:100%;" id="background-pattern">
            <div class="widget-content" id="machine_conveyors" style="height:100%;padding-bottom:40px">
                
            </div>
        </div>
    </div>
    <!-- end -->
      </div>
       <div id="page-elements-shade"></div>   <!-- 遮罩层 -->
    </div>
    <!-- 元素选择区  end -->

    </section>
<input type="hidden" id="imgurl" value="{{ graphicImageUrl('__IMAGEURL__') }}" name="">
<input type="hidden" id="productlineId" value="{{$productlineId}}" name="">


<div class="operationpanel" tabindex="1" style="display:none;position: fixed; top: 0px; width: 100%; height: 100%;bottom: 0px;left: 0">
</div>
<style type="text/css">
    
#content-wrap .edit-helper .tbmenu .tbmitem.active {
    background-color: rgba(0, 0, 0, .8);
}

.table-helper-delmenu {
    position: absolute;
    margin-left: 42px !important;
    margin-top: 160px !important;
}

.menu {
    /*border: 1px solid #DDD;*/
    min-width: 100px;
    /**border:1px solid rgba(0,0,0,.15);**/
    outline: medium none;
    text-align: center;
    text-decoration: none;
    -webkit-user-select: none;
    outline: none;
    box-shadow: 0 6px 12px rgba(0, 0, 0, .2);
    padding: 0px;
}

.menu li {
    list-style-type: none;
    color: #666666;
    background-color: #f6f6f6;
    cursor: pointer;
    height: 33px;
    line-height: 32px;
    font-style: normal;
    font-size: 14px;
    text-align: center;
    /*margin-left:8px;*/
}

.menu li svg {
    width: 1em;
    height: 1em;
}

.menu li:hover,
.menu li:focus {
    background-color: #03a9f4;
    color: #FFFFFF;
}
</style>
<div id="rightclickmenutpl" class="rightclickmenu" style="position:absolute;display:none;z-index: 999999;">
    <ul class="menu">
        <li class="tbmitem coremenu upzindex2top-btn" style="display: none;">
            <!-- <svg class="icon" aria-hidden="true"><use xlink:href="#qlicon-upzindex2top"></use></svg> -->
            <span>置顶</span>
        </li>
        <li class="tbmitem coremenu upzindex-btn" style="display: none;">
            <!-- <svg class="icon" aria-hidden="true"><use xlink:href="#qlicon-upzindex"></use></svg> -->
            <span>上移</span>
        </li>
        <li class="tbmitem coremenu downzindex-btn" style="display: none;">
            <!-- <svg class="icon" aria-hidden="true"><use xlink:href="#qlicon-downzindex"></use></svg> -->
            <span>下移</span>
        </li>
        <li class="tbmitem coremenu downzindex2bottom-btn" style="display: none;">
            <!-- <svg class="icon" aria-hidden="true"><use xlink:href="#qlicon-downzindex2bottom"></use></svg> -->
            <span>置底</span>
        </li>
        <li class="tbmitem coremenu remove-btn" style="display: none;">
            <!-- <svg class="icon" aria-hidden="true"><use xlink:href="#qlicon-trash"></use></svg> -->
            <span>删除</span>
        </li>
    </ul>
</div>
<div class="edit-helper" style="display:none;">
    <div class="bslider left-top zoom-bslide"></div>
    <div class="bslider right-top zoom-bslide"></div>
    <div class="bslider left-bottom zoom-bslide"></div>
    <div class="bslider right-bottom zoom-bslide"></div>
    <!-- <div class="bslider top rotateline zoom-bslide"></div> -->
    <!-- <div class="bslider rotate zoom-bslide" title="旋转"></div> -->
    <div class="bslider top zoom-bslide"></div>
    <div class="bslider right zoom-bslide"></div>
    <div class="bslider bottom zoom-bslide"></div>
    <div class="bslider left zoom-bslide"></div>
    <div class="bslider outline top"></div>
    <div class="bslider outline right"></div>
    <div class="bslider outline bottom"></div>
    <div class="bslider outline left"></div>
<!-- 
    <div class="right tbmenu" style="position:absolute;">
        <a class="tbmitem replace-btn"><i>替换</i></a>
        <a class="tbmitem editcon-btn"><i>编辑</i></a>
        <a class="tbmitem insert-btn"><i>插入</i></a>
        <a class="tbmitem playset-btn" style="overflow:hidden"><i>设置</i></a>
        <a class="tbmitem copy-btn" title="复制" style="overflow:hidden"><i>复制</i></a>
        <a class="tbmitem paste-btn" title="粘贴" style="overflow:hidden"><i>粘贴</i></a>
        <a class="tbmitem upzindex-btn" title="上移一层" style="overflow:hidden"><i>上移</i></a>
        <a class="tbmitem downzindex-btn" title="下移一层" style="overflow:hidden"><i>下移</i></a>
        <a class="tbmitem remove-btn" style="overflow:hidden"><i>删除</i></a>
    </div>
    <div class="right tbmenu table-helper-insertmenu">
        <a class="tbmitem submenu addRowAbove-btn"><i>上边增加行</i></a>
        <a class="tbmitem submenu addRowBelow-btn"><i>下边增加行</i></a>
        <a class="tbmitem submenu addColLeft-btn"><i>左边增加列</i></a>
        <a class="tbmitem submenu addColRight-btn"><i>右边增加列</i></a>
    </div>
    <div class="right tbmenu table-helper-delmenu">
        <a class="tbmitem submenu delThisRow-btn"><i>删除当前行</i></a>
        <a class="tbmitem submenu delThisCol-btn"><i>删除当前列</i></a>
    </div>
    <div class="right tbmenu video-helper-playsetmenu">
        <a class="tbmitem submenu selfmotion-btn"><i>自动播放</i></a>
        <a class="tbmitem submenu handmovement-btn"><i>手动播放</i></a>
        <a class="tbmitem submenu fullscreen-btn"><i>全屏</i></a>
        <a class="tbmitem submenu unfullscreen-btn"><i>非全屏</i></a>
    </div> -->
</div>

<div class="item elem bluetpl_panel animated zoomIn tpl-tool" elem-id="tool" elem-category="tool" data-id="" id="" draggable="true" style="display: none;">
    <div class="elem-wrap" data-move="true" data-elemid="tool" data-type="element">
        <div class="machineitem btn-group">
            <div class="imgplus ">
                <img src="" height="120" width="120">
            </div>
        </div>
        <img class="element machineelem" src="" height="120" width="120" style="display:none;max-width:100%;"/>
    </div>
    <div class="machinename" style="width: 120px; height: auto; margin: 0 15px; text-align: center;"></div>
</div>

<div class="item elem bluetpl_panel animated zoomIn tpl-conveyor" elem-id="conveyor" elem-category="conveyor" data-id="" id="" draggable="true" style="display: none;">
    <div class="elem-wrap" data-move="true" data-elemid="conveyor" data-type="element">
        <div class="machineitem btn-group">
            <div class="imgplus ">
                <img src="" height="120" width="120">
            </div>
        </div>
        <img class="element machineelem" src="" height="120" width="120" style="display:none;max-width:100%;"/>
    </div>
    <div class="machinename" style="width: 120px; height: auto; margin: 0 15px; text-align: center;"></div>
</div>
<style type="text/css">
    #settingmodal .form-group{clear: both;margin-bottom: 10px;height: 35px;}
    #settingmodal .control-label{text-align: left;}
</style>
<!-- 设置 start -->
<div class="modal fade" id="settingmodal" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
  <div class="control large e modal-dialog">
    <div class="modal-content">
        <div class="modal-header"> <a class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
            <h4 class="modal-title">生产线设置</h4>
        </div>
        <div class="modal-body" style="height: 300px;">
        
          <div class="settingform">
            <div class="row">
            <div class="col-md-12"></div>
            <div class="fields-group">
                <div class="form-group  ">
                    <label for="name" class="col-sm-3  control-label">生产线名称</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                            <input type="text" id="name" name="name" value="" class="form-control name" placeholder="输入 生产线名称">
                        </div>
                    </div>
                </div>
                <div class="form-group  ">
                    <label for="opcHostIP" class="col-sm-3  control-label">OPC 主机地址</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                            <input type="text" id="opcHostIP" name="opcHostIP" value="" class="form-control opcHostIP" placeholder="输入 OPC 主机地址">
                        </div>
                    </div>
                </div>
                <div class="form-group  ">
                    <label for="opcServerName" class="col-sm-3  control-label">OPC 主机名称</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                            <input type="text" id="opcServerName" name="opcServerName" value="" class="form-control opcServerName" placeholder="输入 OPC 主机名称">
                        </div>
                    </div>
                </div>
                <div class="form-group  ">
                    <label for="shortDownTimes" class="col-sm-3  control-label">小停机时长(分钟)</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                            <input type="text" id="shortDownTimes" name="shortDownTimes" value="" class="form-control shortDownTimes initialized" placeholder="输入 小停机时长(分钟)">
                        </div>
                    </div>
                </div>
                <div class="form-group  ">
                    <label for="longDownTimes" class="col-sm-3  control-label">大停机时长(分钟)</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                            <input type="text" id="longDownTimes" name="longDownTimes" value="" class="form-control longDownTimes initialized" placeholder="输入 小停机时长(分钟)">

                            <!-- <div class="input-group"><span class="input-group-btn"><button type="button" class="btn btn-primary">-</button></span><input style="width: 100px; text-align: center;" type="text" id="longDownTimes" name="longDownTimes" value="120" class="form-control longDownTimes initialized" placeholder="输入 大停机时长(分钟)"><span class="input-group-btn"><button type="button" class="btn btn-success">+</button></span></div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        <div class="buttons" style="text-align: center;    padding-bottom: 20px;">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin: 0 10px;">取消</button>
                <button type="button" class="btn btn-primary confirmdelbtn" style="margin: 0 10px;">保存</button>
          </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- 设置 end -->

    <script type="text/javascript">

        $(function(){
            var headerWidth = $('.content-wrapper').width() - 300;
            $('.content-header').width(headerWidth+'px');
            var productlineId = {{$productlineId}};
            contenteditor = new contenteditor();
            new productline(productlineId,contenteditor);
            contenteditor.init();

        });

        function getElementHelperHtml(){
            return $($('.edit-helper').prop('outerHTML')).css('display', '').prop('outerHTML');
        }


    </script>
@endsection