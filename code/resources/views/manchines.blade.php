<span class="hidden-xs" style="margin-left: 10px;margin-right: 20px;">生产线：{{$productName}}</span>

<select class="form-control manchinesradio" style="width: 10%;display: inherit;">
    @foreach($options as $select => $option)
        <option value="{{$select}}" {{ \Request::get('mId') == $select ? 'selected' : '' }}>{{$option}}</option>
    @endforeach
</select>

<div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{{$createUrl}}" class="btn btn-sm btn-success" title="新增">
        <i class="fa fa-save"></i><span class="hidden-xs">&nbsp;&nbsp;新增</span>
    </a>
</div>