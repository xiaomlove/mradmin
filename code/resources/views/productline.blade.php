<div class="btn-group productlineradio" data-toggle="buttons">
    @foreach($options as $option => $label)
    <label class="btn btn-default btn-sm {{ \Request::get('productline') == $option ? 'active' : '' }}">
        <input type="radio" class="manchine-productline" value="{{ $option }}">{{$label}}
    </label>
    @endforeach
</div>

<div class="btn-group pull-right" style="margin-right: 10px">
    <a href="/admin/machine/create?productline={{ \Request::get('productline') }}" class="btn btn-sm btn-success" title="新增">
        <i class="fa fa-save"></i><span class="hidden-xs">&nbsp;&nbsp;新增</span>
    </a>
</div>