<li class="dd-item" data-id="{{ $branch[$keyName] }}">
    <div class="dd-handle">
        {!! $branchCallback($branch) !!}
        <span class="pull-right dd-nodrag">
            <a href="{{ $path }}/{{ $branch[$keyName] }}/edit?productlineId={{ $branch['productlineId'] }}"><i class="fa fa-edit"></i></a>
            <a href="javascript:void(0);" data-id="{{ $branch[$keyName] }}" class="tree_branch_delete"><i class="fa fa-trash"></i></a>
            <a href="{{ route('admin.fault-desc.index', ['stopreasonId' => $branch['id']]) }}" target="_blank">&nbsp;&nbsp;故障</a>
        </span>
    </div>
    @if(isset($branch['children']))
        <ol class="dd-list">
            @foreach($branch['children'] as $branch)
                @include($branchView, $branch)
            @endforeach
        </ol>
    @endif
</li>