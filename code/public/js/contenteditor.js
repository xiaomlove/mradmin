/**
 * 
 */

(function(){
	var root = this;

	var contenteditor = function(){
        this.activeElem = null;
        this.commandElem = null;
        this.productlineId = $('#productlineId').val();
		this.contentBody = $('#concontainer');
		this.contentBodyDom = document.getElementById('concontainer');
		this.rightClickMenu;
		this.bodyMouseoverListeners = [];
		this.setContentPosition();
		this.bindDragEvent();
        var panelObj = self = this;
        this.init = function(){
            // this.setOperationPanel();
            this.loadProductGraphic();
            this.bindSaveEvent();
            this.bingSettingEvent();
            this.setContentRightDown();
            this.setElemMoveEable();
        }

        this.loadProductGraphic = function(){
            $.ajax({
                url: '/admin/productline/getGraphic',
                dataType: 'JSON',
                data: {'productlineId':self.productlineId},
                type: 'get',
                async: false,
                success: function(data){
                    if (data.graphic) {
                        self.contentBody.find('#content-wrap').append(data.graphic);
                        self.bindCommonEventOfCon();
                    }
                }
            });
        }

        this.bindCommonEventOfCon = function(elem){
             self.contentBody.find('.elem-wrap').each(function(){
                var helperdom = getElementHelperHtml();
                $(this).append(helperdom);
            });
            
            
        }

        this.bindSaveEvent = function(){
            var saveBtn = $('.savebtn');
            saveBtn.on('click',function(){
                self.contentBody.find(".elem-wrap").removeClass("elem-wrap-selected");
                self.removeCommandElem();//移动编辑中元素
                self.activeElem = null;
                var contentData = self.getContent();
                $.ajax({
                    url: '/admin/productline/saveContent',
                    dataType: 'JSON',
                    data: {'productlineId':self.productlineId,'content':contentData},
                    type: 'get',
                    async: false,
                    success: function(data){
                        
                    }
                });

            })
        }

        this.getContent = function(){
            var content = $('#content-wrap').clone(true);
            content.find('.edit-helper').remove();
            return content.prop('innerHTML');
            // content.find('.machineitem').remove(); 
            // content.find('.machineelem').show();
            // var helperdom = getElementHelperHtml();
            // content.append(helperdom);
            // var conElem = content.prop('outerHTML');
        }

        this.bingSettingEvent = function(){
            var _this = this;
            var settingBtn = $('.settingsbtn');
            settingBtn.on('click',function(){
                self.contentBody.find(".elem-wrap").removeClass("elem-wrap-selected");
                self.removeCommandElem();//移动编辑中元素
                self.activeElem = null;
                self.loadProductSettings();

                $('#settingmodal').find('.confirmdelbtn').on('click',function(){
                    var settingmodal = $('#settingmodal');
                    var name = settingmodal.find('#name').val();
                    var opcHostIP = settingmodal.find('#opcHostIP').val();
                    var opcServerName = settingmodal.find('#opcServerName').val();
                    var shortDownTimes = settingmodal.find('#shortDownTimes').val();
                    var longDownTimes = settingmodal.find('#longDownTimes').val();
                    $.ajax({
                        url: '/admin/productline/savesettings',
                        dataType: 'JSON',
                        data: {
                            'productlineId':self.productlineId,
                            'name':name,
                            'opcHostIP':opcHostIP,
                            'opcServerName':opcServerName,
                            'shortDownTimes':shortDownTimes,
                            'longDownTimes':longDownTimes,
                        },
                        type: 'get',
                        async: false,
                        success: function(data){
                            if (data.ret == -1){
                                settingmodal.modal('hide');
                            }
                        }
                    });
                })
            })
        }

        this.loadProductSettings = function(){
            var settingmodal = $('#settingmodal');
            $.ajax({
                url: '/admin/productline/getProductline',
                dataType: 'JSON',
                data: {'productlineId':self.productlineId},
                type: 'get',
                async: false,
                success: function(data){
                    if (!data.productline) {return;}
                    var productline = data.productline;
                    settingmodal.find('#name').val(productline.name);
                    settingmodal.find('#opcHostIP').val(productline.opcHostIP);
                    settingmodal.find('#opcServerName').val(productline.opcServerName);
                    settingmodal.find('#shortDownTimes').val(productline.shortDownTimes);
                    settingmodal.find('#longDownTimes').val(productline.longDownTimes);
                    $('#settingmodal').modal('show');
                }
            });
        }

        this.setContenteditorWindowFocus = function(){
            this.contentBody.focus();
        }
        
        this.setOperationPanel = function(){
            //辅助遮罩层
            var operationpanel = $($('.operationpanel').prop('outerHTML'));
            this.contentBody.append(operationpanel);
            this.contentBody.find('.operationpanel').css('display','block');
        }
        //去掉命令，也即双击后点击了body或其他元素
        this.removeCommandElem = function(){
            self.commandElem = self.activeElem;
            if (self.commandElem !== null){
                self.commandElem.attr("data-move", true).blur();
                // mainEditor.zntoolbar.disableCommand();
                // self.disableStyleList();
                
                // var elemId = self.commandElem.attr("data-elemid");
                // var element = elements.getElement(elemId);
                // element.onBlurEvent(self.commandElem);
                self.elemOnblurEvent(self.commandElem);
                //同时去掉列表的删除事件
                self.commandElem.find("li").off().removeClass("selected").blur();
            }
        }

        this.elemOnblurEvent = function(elem){

        }

        this.addBodyMousemoveListener = function(listener){
            this.bodyMouseoverListeners.push(listener);
            return this.bodyMouseoverListeners.length-1;
        }

        this.removeBodyMouseoverListener = function(i){
            var listeners = this.bodyMouseoverListeners;
            var len = listeners.length;
            if(i<0 || i>=len){
                return;
            }
            delete this.bodyMouseoverListeners[i];
        }

        this.setContentRightDown = function(){
        	var _this = this;
            //右键菜单
            var menu = this.rightClickMenu = $($('#rightclickmenutpl').attr('id','rightclickmenu').prop('outerHTML'));
            // this.contentBody.append(this.rightClickMenu);
            var scrollable = $('#scrollable-content');
            scrollable.find('.canvas').append(this.rightClickMenu);


            this.contentBody.bind("mousedown", function(ev) {
                var evTarget = ev.target;
                var $evTarget = $(evTarget);
                 _this.isElemWrap = $evTarget.hasClass('operationpanel')?false:true;
                var isContentEditable = $evTarget.hasClass('contenteditable')?true:false;
                if(ev.which == 3){
                    var e = ev || window.ev;
                    var contentEditorLeft =  _this.contentBody.offset().left - $('#slidescroll').width();
                    var contentEditorTop  =  _this.contentBody.offset().top - $('#page-heaer').height();
                    var rightedge = _this.getPageX(ev)+1;
                    var bottomedge = _this.getPageY(ev)+1;
                    var rightedge = rightedge + contentEditorLeft;
                    var bottomedge = bottomedge + contentEditorTop;

                    if (!_this.isElemWrap) {
                        _this.hideRightClickMenu();
                    }
                    if (isContentEditable) {
                        return true;
                    }

                    // if(clipboard.copyInfo){
                    //     menu.find('.paste-btn').off('click').on('click', function(ev){
                    //         clipboard.pasteElement(ev);
                    //         _this.hideRightClickMenu();
                    //         return false;
                    //     }).css('display', 'block');
                    // }

                    menu.off('mouseleave').on('mouseleave', function(e){ 
                        _this.hideRightClickMenu();
                        return false;
                    });

                    menu.off('mouseover').on('mouseover', function(e){ 
                        _this.showRightClickMenu();
                        return false;
                    });
                    var lena = _this.addBodyMousemoveListener(function(ev){
                        var left = parseInt(menu.css('left'));
                        var right = left+menu.width();
                        var top = parseInt(menu.css('top'));
                        var bottom = top+menu.height();
                        var secondmenu = $('.secondmenu:visible');
                        // console.log(rightedge+'---'+left);
                        // console.log(bottomedge+'---'+top);
                        //判断是否二级菜单正在显示
                        if (secondmenu.length == 0) {
                            if(rightedge>=left-20 && rightedge<=right+20 && bottomedge>=top-20 && bottomedge<=bottom+20){
                                return true;
                            }
                        }else{
                            var sm = {};
                            sm.left = right + parseInt(secondmenu.css('left'));
                            sm.right = sm.left + secondmenu.width();
                            sm.top = top + parseInt(secondmenu.css('top'));
                            sm.bottom = sm.top+secondmenu.height();
                            if ((rightedge>=left-20 && rightedge<=right+20 && bottomedge>=top-20 && bottomedge<=bottom+20)
                             || (rightedge>=sm.left-20 && rightedge<=sm.right+20 && bottomedge>=sm.top-20 && bottomedge<=sm.bottom+20) 
                             ) 
                            {
                                return true;
                            }
                        }
                        _this.removeBodyMouseoverListener(lena);
                        _this.hideRightClickMenu();
                        return true;
                    });
                    var winHeight = parseInt($(window).height());//浏览器高度
                    var menuHeight = parseInt(menu.css('height'));
                    // var contentEditorLeft =  parseInt(_this.contentBody.css('margin-left'));
                    console.log('9909090');
                    console.log('rightedge:'+rightedge);
                    console.log('bottomedge:'+bottomedge);
                    console.log('menuHeight:'+menuHeight);
                    console.log('winHeight:'+winHeight);


                    if ((bottomedge + menuHeight) > winHeight) {
                        menu.css("left",rightedge).css("top",bottomedge - menuHeight);
                    }else{
                        menu.css("left",rightedge).css("top",bottomedge);
                    }
                    _this.showRightClickMenu();
                    return false;
                }
            })
        }

        this.bindContainerMouseEvent = function(){
            var _this = this;

            $('body').find('#page-thumb, #page-canvas #scrollable-content, #page-elements').on('click', function(){
                _this.setContenteditorWindowFocus();
            });


            this.setContenteditorWindowFocus = function(){
                _this.contentBody.focus();
            }

            this.contentBody.on('mousemove', function(ev){

                _this.eachObj(_this.bodyMouseoverListeners, function(listener){
                    if(!listener){
                        return true;
                    }
                    listener(ev);
                });
                return true;
            });
        }

        this.eachObj = function(loopable,callback,self){
			var additionalArgs = Array.prototype.slice.call(arguments, 3);
			if (loopable){
				if (loopable.length === +loopable.length){
					var i;
					for (i=0; i<loopable.length; i++){
						callback.apply(self,[loopable[i], i].concat(additionalArgs));
					}
				}
				else{
					for (var item in loopable){
						callback.apply(self,[loopable[item],item].concat(additionalArgs));
					}
				}
			}
		};

        //设置元素可拖动与缩放，删除。这些每个元素都必须的共同操作。
        this.setElemMoveEable = function(){
            var _this = this;
            this.contentBody.on("click", ".edit-helper .remove-btn", function(e){
                var $wrap =  $(this).parents(".elem-wrap");
                _this.removeElemFromDom($wrap);
            });

            //缩放与移动
            var $elemWrap, elemWrap, $elem, oldX, oldY, oldWidth, oldHeight, oldMouseX, oldMouseY, isDown = false, direction, isMove=false;
            var RX, RY;//旋转圆心坐标
            var oldDeg;//原角度
            var oldPointDeg;//起点角度
            var oldoldangle;//旋转前的角度
            var changeDeg; //旋转改变的角度
            var angleRegion; //偏转级别
            var isMoveBykey = false;
            var mouseStartX,mouseStartY;

            //var bodyWidth = this.contentBody.width(), bodyHeight = this.contentBody.height();
            var bodyWidth = this.contentBody.width(), bodyHeight = this.contentBody.height();
            function getOffset(current, type){
                return current;
            }

            function getDeg(x, y){
                var out;
                if (x == 0){
                    if (y >= 0){
                        out = Math.PI*3/2;
                    }else{
                        out = Math.PI/2;
                    }
                }else{
                    var result = Math.abs(Math.atan(y/x));//逆时针为负，顺时针为正。统一顺时针看
                    if (x >0 && y < 0){
                        out = result;
                    }else if(x < 0 && y < 0){
                        out = Math.PI - result;
                    }else if(x < 0 && y >= 0){
                        out = Math.PI + result;
                    }else{
                        out = Math.PI*2 - result;
                    }
                } 
                return radToDeg(out);
            }

          /** 解析matrix矩阵，0°-360°，返回旋转角度 
            * 当a=b||-a=b,0<=deg<=180 
            * 当-a+b=180,180<=deg<=270 
            * 当a+b=180,270<=deg<=360 
            * 
            * 当0<=deg<=180,deg=d; 
            * 当180<deg<=270,deg=180+c; 
            * 当270<deg<=360,deg=360-(c||d); 
            **/
            this.getmatrix = function(thematrix){ 
                var values = thematrix.split('(')[1].split(')')[0].split(',');
                var a = values[0];
                var b = values[1];
                var c = values[2];
                var d = values[3]; 
                var e = values[4]; 
                var f = values[5]; 

                var aa=Math.round(180*Math.asin(a)/ Math.PI);  
                var bb=Math.round(180*Math.acos(b)/ Math.PI);  
                var cc=Math.round(180*Math.asin(c)/ Math.PI);  
                var dd=Math.round(180*Math.acos(d)/ Math.PI);  
                var deg=0;  
                if(aa==bb||-aa==bb){  
                    deg=dd;  
                }else if(-aa+bb==180){  
                    deg=180+cc;  
                }else if(aa+bb==180){  
                    deg=360-cc||360-dd;  
                }  
                // deg = deg>=360?0:deg;
                // var red = degToRad(deg);
                // return red;
                return deg = deg>=360?0:deg; 
            }

            function degToRad(deg){
                var red;
                red = (deg * Math.PI) / 180;
                return red;
            }

            function radToDeg(rad){
                var deg;
                deg = (rad * 180) / Math.PI;
                return deg;
            }           

            this.setElemActive = function(elemDom){
                if (this.activeElem !== null){
                    if(this.activeElem.attr('id') == elemDom.attr('id')){
                        return;
                    }
                    this.activeElem.find("[contenteditable=true]").blur();
                }else{
                }
                this.removeCommandElem();
                this.activeElem = elemDom;
                this.contentBody.find("[data-move=true]").removeClass("elem-wrap-selected");
                elemDom.addClass("elem-wrap-selected");
                elemDom.find('.zoom-bslide').show();
                var domId = this.activeElem.attr('id');
                this.activeElem.addClass("elem-wrap-selected");
            }

            this.contentBody.bind("mousedown", function(ev){
                // if (_this.isChoosingElem == true) {
                //     return false;
                // }
                var evTarget = ev.target;
                var $evTarget = $(evTarget);
                var evTargetName = evTarget.tagName.toLowerCase();
                // if (_this.isElemWrap == true) {
                //     return false;
                // }
                var isContainer = $evTarget.hasClass('concontainer')?true:false;
                
                if(isContainer){
                    //去掉编辑辅助框
                    self.contentBody.find(".elem-wrap").removeClass("elem-wrap-selected");
                    self.removeCommandElem();//移动编辑中元素
                    
                    self.activeElem = null;
                    // self.notify();
                }
            }); 



            var moveStartPosition, moveEndPosition;

            this.contentBody.on("mousedown click mouseup", "[data-move=true] .element", function(e){
                $elemWrap = $(this).parents(".elem-wrap");
                elemWrap = $elemWrap.get(0);

                e.preventDefault();
                if (e.type === "mousedown"){
                    _this.isElemWrap = true;
                    moveStartPosition = {left: parseInt($elemWrap.css('left')), top: parseInt($elemWrap.css('top'))};
                    if (e.which ==3) {
	                    self.setElemActive($elemWrap);
	                    _this.setContenteditorWindowFocus();
                        var domId = _this.activeElem.attr('id');
                        // var element = elements.getConElement(domId);
                        self.showRightClickMenu();
                        return;
                    }
                    isDown = true;
                    isMove = false;
                    oldX = parseFloat(document.defaultView.getComputedStyle(elemWrap, null).left);
                    oldY = parseFloat(document.defaultView.getComputedStyle(elemWrap, null).top);
                    oldMouseX = _this.getPageX(e);
                    oldMouseY = _this.getPageY(e);

                    self.setElemActive($elemWrap);
                    _this.setContenteditorWindowFocus();

                }else if (e.type === "click"){
                    event.stopPropagation(); // 阻止事件冒泡
                    return false;
                    // var element = _this.getElemOfActiveObj();
                    // element && element.onClick();
                    // self.notify();
                }else if(e.type === "mouseup"){
                    if(!$elemWrap || !self.activeElem){
                        return;
                    }
                    if (e.which ==3) {
                        return;
                    }
                    moveEndPosition = {left: parseInt($elemWrap.css('left')), top: parseInt($elemWrap.css('top'))};
                    // if(moveEndPosition.left!=moveStartPosition.left || moveEndPosition.top!=moveStartPosition.top){
                    //     var Command = undoRedo.getCommand("moveElement");
                    //     var dc = new Command();
                    //     var domId = self.activeElem.attr('id');
                    //     var element = elements.getConElement(domId);
                    //     dc.setChangeObject(slidePanel.getActiveSlide(), element, moveStartPosition, moveEndPosition);
                    //     undoRedo.insertUndoCommand(dc);
                    // }
                }
            });
            
            function switchEditCommands(activeElem){
                var editCommands = activeElem.editCommands;
            }

            

            var elemWrapSizeOnMDown, elemWrapSizeOnMUp, elemDomsSizeOnScale;

            this.contentBody.on("mousedown", ".bslider", function(e){  
                _this.isElemWrap = true;
                e.stopPropagation();
                e.preventDefault();
                // isDown = true;
                direction = this.classList.item(1);

                $elemWrap = $(this).parents(".elem-wrap");
                elemWrap = $elemWrap.get(0);
                $elem = $elemWrap.find(".element");

                var elemId = $elemWrap.attr("data-elemid");
                var element = elements.getElement(elemId);
                var conElem = elements.getConElement($elemWrap.attr("id"));
                if(!conElem){
                    return true;
                }
                if (!conElem.isReSizeable()) {
                    isDown = false;
                    conElem.dom.addClass('hint--top hint--rounded');
                    conElem.dom.attr('aria-label','当前元素不支持缩放');
                }else{
                    isDown = true;
                }

                oldWidth = $elemWrap.width();
                oldHeight = $elemWrap.height();
                oldMouseX = _this.getPageX(e);
                oldMouseY = _this.getPageY(e);
                oldX = parseFloat(document.defaultView.getComputedStyle(elemWrap, null).left);
                oldY = parseFloat(document.defaultView.getComputedStyle(elemWrap, null).top);
                RX = oldX + oldWidth/2;    //中心点
                RY = oldY + oldHeight/2;
                oldPointDeg = getDeg(_this.getPageX(e) - RX, RY - _this.getPageY(e));
                oldDeg = $elemWrap.css("transform");//矩阵matrix值，暂时不管了
                if (oldDeg == 'none') {
                    oldDeg = 'matrix(0, 0, 0, 0, 0, 0)';
                }
                oldoldangle = panelObj.getmatrix(oldDeg);
                angleRegion = _getAngleRegion(oldoldangle);
                elemWrapSizeOnMDown = {width: oldWidth, height:oldHeight, left:parseFloat($elemWrap.css("left")), top:parseFloat($elemWrap.css("top")), transform: $elemWrap.css("transform")};
                // console.log("elemWrapSizeOnMDown.rotate:"+elemWrapSizeOnMDown.rotate);
                elemDomsSizeOnScale = {};
                $elemWrap.find('.whscaleable').each(function(){
                    var scaleObj = $(this);
                    scaleObj.attr("widthondown", scaleObj.width()).attr("heightondown", scaleObj.height());
                    var xpath = helpers.getXPathOfDom("#"+$elemWrap.attr('id'), $(this));
                    elemDomsSizeOnScale[xpath] = {origSize:{width:scaleObj.width(), height:scaleObj.height()}};
                });
                $elemWrap.find('.whscaleableHotspotImage').each(function(){
                    var scaleObj = $(this);
                    scaleObj.attr("transformxondown", scaleObj.attr('transformx')).attr("transformyondown", scaleObj.attr('transformy'));
                });
            }).on("mouseup", ".bslider", function(){
            });

            var _scaleSameRatio = function($elemWrap, dx, dy){
                var len = $elemWrap.find('.whscaleable').length;
                var scaleX = Math.round(dx/len);
                var scaleY = Math.round(dy/len);
                $elemWrap.find('.whscaleable').each(function(){
                    var scaleObj = $(this);
                    scaleObj.width(Math.round(scaleObj.attr('widthondown'))+scaleX);
                    scaleObj.height(Math.round(scaleObj.attr('heightondown'))+scaleY);
                });
                $elemWrap.find('.whscaleableHotspotImage').each(function(){
                    var scaleObj = $(this);
                    // 计算扩大的百分比
                    var ratiox = dx / $elemWrap.find('.whscaleable').attr('origw');
                    var ratioy = dy / $elemWrap.find('.whscaleable').attr('origh');
                    var nowtransformx = parseInt(scaleObj.attr('transformxondown')) * (1 + ratiox);
                    var nowtransformy = parseInt(scaleObj.attr('transformyondown')) * (1 + ratioy);
                    scaleObj.css('transform','translate3d('+nowtransformx+'px, '+nowtransformy+'px, 0px)');
                    scaleObj.attr('transformx',nowtransformx);
                    scaleObj.attr('transformy',nowtransformy);
                });
            }

            var _scaleWidthRatio = function($elemWrap, dx){
                var len = $elemWrap.find('.whscaleable').length;
                var scaleX = Math.round(dx/len);
                $elemWrap.find('.whscaleable').each(function(){
                    var scaleObj = $(this);
                    scaleObj.width(Math.round(scaleObj.attr('widthondown'))+scaleX);
                });
            }

            var _scaleHeightRatio = function($elemWrap, dy){
                var len = $elemWrap.find('.whscaleable').length;
                var scaleY = Math.round(dy/len);
                $elemWrap.find('.whscaleable').each(function(){
                    var scaleObj = $(this);
                    scaleObj.height(Math.round(scaleObj.attr('heightondown'))+scaleY);
                });
            }

            this._updateCursor = function($elemWrap, newangle, oldangle){

                oldangle = oldangle || null;
                //这里还需要初始化操作。
                //当初次加载时，没有oldangle
                var newAngleRegion =  _getAngleRegion(newangle);
                if (oldAngleRegion != null) {
                    var oldAngleRegion = _getAngleRegion(oldangle);
                    if (newAngleRegion == oldAngleRegion) {
                        return;
                    }
                }

                var editHelper = $elemWrap.find('.edit-helper');
                if (newAngleRegion == 1) {
                    editHelper.find('.left-top').css({cursor:'nw-resize'});
                    editHelper.find('.right-top').css({cursor:'ne-resize'});
                    editHelper.find('.left-bottom').css({cursor:'ne-resize'});
                    editHelper.find('.right-bottom').css({cursor:'nw-resize'});
                    editHelper.find('.top').css({cursor:'n-resize'});
                    editHelper.find('.right').css({cursor:'e-resize'});
                    editHelper.find('.bottom').css({cursor:'n-resize'});
                    editHelper.find('.left').css({cursor:'e-resize'});
                }
                else if (newAngleRegion == 2) {
                    editHelper.find('.left-top').css({cursor:'ne-resize'});
                    editHelper.find('.right-top').css({cursor:'nw-resize'});
                    editHelper.find('.left-bottom').css({cursor:'nw-resize'});
                    editHelper.find('.right-bottom').css({cursor:'ne-resize'});
                    editHelper.find('.top').css({cursor:'e-resize'});
                    editHelper.find('.right').css({cursor:'n-resize'});
                    editHelper.find('.bottom').css({cursor:'e-resize'});
                    editHelper.find('.left').css({cursor:'n-resize'});
                }
                else if (newAngleRegion == 3) {
                    editHelper.find('.left-top').css({cursor:'nw-resize'});
                    editHelper.find('.right-top').css({cursor:'ne-resize'});
                    editHelper.find('.left-bottom').css({cursor:'ne-resize'});
                    editHelper.find('.right-bottom').css({cursor:'nw-resize'});
                    editHelper.find('.top').css({cursor:'n-resize'});
                    editHelper.find('.right').css({cursor:'e-resize'});
                    editHelper.find('.bottom').css({cursor:'n-resize'});
                    editHelper.find('.left').css({cursor:'e-resize'});
                }
                else if (newAngleRegion == 4) {
                    editHelper.find('.left-top').css({cursor:'ne-resize'});
                    editHelper.find('.right-top').css({cursor:'nw-resize'});
                    editHelper.find('.left-bottom').css({cursor:'nw-resize'});
                    editHelper.find('.right-bottom').css({cursor:'ne-resize'});
                    editHelper.find('.top').css({cursor:'e-resize'});
                    editHelper.find('.right').css({cursor:'n-resize'});
                    editHelper.find('.bottom').css({cursor:'e-resize'});
                    editHelper.find('.left').css({cursor:'n-resize'});
                }
            }


            var _getAngleRegion = function(angle){
                var angleRegion;
                if ((angle >=0 && angle <45) || angle >=315 && angle <360) {
                    angleRegion = 1;
                }
                else if (angle >=45 && angle <135) {
                    angleRegion = 2;
                }
                else if (angle >=135 && angle <225) {
                    angleRegion = 3;
                }
                else if (angle >=225 && angle <315) {
                    angleRegion = 4;
                }
                return angleRegion;
            }



            var mouseUpOnScaled = function(direction){
                if(!$elemWrap){
                    return;
                }
                elemWrapSizeOnMUp = {width: $elemWrap.width(), height:$elemWrap.height(), left:parseFloat($elemWrap.css("left")), top:parseFloat($elemWrap.css("top")), transform: $elemWrap.css("transform")};
                $elemWrap.find('.whscaleable').each(function(){
                    $(this).removeAttr("widthondown").removeAttr("heightondown");
                    var xpath = helpers.getXPathOfDom($elemWrap, $(this));
                    if(elemDomsSizeOnScale[xpath]){
                        elemDomsSizeOnScale[xpath]["targetSize"] = {width:$(this).width(), height:$(this).height()};
                    }
                });
                $elemWrap.find('.whscaleableHotspotImage').each(function(){
                    $(this).removeAttr("transformxondown").removeAttr("transformyondown");
                });

                // if(elemWrapSizeOnMDown.width!=elemWrapSizeOnMUp.width || elemWrapSizeOnMDown.height!=elemWrapSizeOnMUp.height || elemWrapSizeOnMDown.transform!=elemWrapSizeOnMUp.transform){
                //     var Command = undoRedo.getCommand("scaleElement");
                //     var dc = new Command();
                //     dc.setChangeObject(slidePanel.getActiveSlide(), $elemWrap, direction, {origSize:elemWrapSizeOnMDown, targetSize:elemWrapSizeOnMUp}, elemDomsSizeOnScale);
                //     undoRedo.insertUndoCommand(dc);
                // }
            }
            
            var positionBeforMove = {};
            
            this.contentBody.on("mousemove mouseup mouseleave keydown keyup", function(e){
                // var _this = this;
                if (e.type === "mouseup" || e.type === "mouseleave"){
                    if (_this.isDownConter) {
                        _this.isDownConter = false;
                        if (_this.activeElem !== null){
                            $elemWrap = $(this).parents(".elem-wrap");
                        }
                        return;
                    }
                    if(direction){
                        mouseUpOnScaled(direction);

                        if (direction == 'rotate') {
                            panelObj._updateCursor($elemWrap, changeDeg, oldoldangle);
                        }
                    }
                    isDown = false;
                    direction = null;
                    
                }
                else if(e.type === "mousemove"){
                    if (_this.isDownConter) {
                        return;
                    }
                    if (!isDown){
                        return;
                    }
                    isMove = true;
                    var dx = 0, dy = 0;//增加的量
                    //console.log("mousemovemousemovemousemovemousemovemousemovemousemove:"+direction);
                    switch (direction){
                        case "left"://左
                            if (angleRegion == 1) {
                                dx =  oldMouseX - _this.getPageX(e);//e.pageX;
                                var pl = -dx + oldX;
                                
                                // pl = pl<0 ? 0 : pl;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                    //height: $elemWrap.height(),
                                    left: pl,
                                });
                                _scaleWidthRatio($elemWrap, dx);
                            }
                            else if (angleRegion == 2) {
                                dy =  oldMouseY - _this.getPageY(e);
                                $elemWrap.css({
                                    width: dy + oldWidth,
                                    top: -dy/2 + oldY,
                                    left:oldX - dy/2,
                                });
                                _scaleWidthRatio($elemWrap, dy);
                            }
                            else if (angleRegion == 3) {
                                dx = oldMouseX - _this.getPageX(e);//e.pageX;
                                $elemWrap.css({
                                    width: -dx + oldWidth,
                                });
                                _scaleWidthRatio($elemWrap, -dx);
                            }
                            else if (angleRegion == 4) {
                                dy =  _this.getPageY(e) - oldMouseY;
                                $elemWrap.css({
                                    width: dy + oldWidth,
                                    top: dy/2 + oldY,
                                    left:oldX - dy/2,
                                });
                                 _scaleWidthRatio($elemWrap, dy);
                            }
                            break;
                        case "top"://上
                            if (angleRegion == 1) {
                                dy =  oldMouseY - _this.getPageY(e);//_this.getPageY(e);
                                $elemWrap.css({
                                    height: dy + oldHeight,
                                    top: -dy + oldY,
                                });
                                _scaleHeightRatio($elemWrap, dy);
                            }
                            else if (angleRegion == 2) {
                                dx = oldMouseX -  _this.getPageX(e);//e.pageX;
                                $elemWrap.css({
                                    height: -dx + oldHeight,
                                    top: dx/2 + oldY,
                                    left: oldX - dx/2,
                                    //width: $elemWrap.width(),
                                });
                                _scaleHeightRatio($elemWrap, -dx);
                            }else if (angleRegion == 3) {
                                dy =  _this.getPageY(e) - oldMouseY;
                                $elemWrap.css({
                                    height: dy + oldHeight,
                                });
                                _scaleHeightRatio($elemWrap, dy);
                            }
                            else if (angleRegion == 4) {
                                dx = oldMouseX - _this.getPageX(e);//e.pageX;
                                $elemWrap.css({
                                    height: dx + oldHeight,
                                    top: -dx/2 + oldY,
                                    left: oldX - dx/2,
                                });
                                _scaleHeightRatio($elemWrap, dx);
                            }
                            
                            /**
                            $elem.css({
                                height: dy + oldHeight,
                                //width: $elem.width(),
                            });**/
                            break;
                        case "left-top"://左上
                            //以横向为基准等比缩放
                            if (angleRegion == 1) {
                                dx = oldMouseX - _this.getPageX(e);//e.pageX;
                                dy = dx*oldHeight/oldWidth;
                                var pl = -dx + oldX;
                                // pl = pl<0 ? 0 : pl;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                    height: dy + oldHeight,
                                    left: pl,
                                    top: -dy + oldY,
                                });
                                _scaleSameRatio($elemWrap, dx, dy);
                            }
                            else if (angleRegion == 2) {
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                var pl = -dx + oldX;
                                // pl = pl<0 ? 0 : pl;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                    height: dy + oldHeight,
                                    top: -dx + oldY,
                                });
                                _scaleSameRatio($elemWrap, dx, dy);
                            }
                            else if (angleRegion == 3) {
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                    height: dy + oldHeight,
                                });
                                _scaleSameRatio($elemWrap, dx, dy);
                            }
                            else if (angleRegion == 4) {
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: -dx + oldWidth,
                                    height: -dy + oldHeight,
                                    left: dx + oldX,
                                });
                                _scaleSameRatio($elemWrap, -dx, -dy);
                            }
                            /**
                            $elem.css({
                                width: dx + oldWidth,
                                height: dy + oldHeight,
                            });**/
                            break;
                        case "left-bottom"://左下
                            //以横向为基准等比缩放
                            if (angleRegion == 1) {
                                //dx = oldMouseX - e.pageX;
                                dx = oldMouseX - _this.getPageX(e);
                                dy = dx*oldHeight/oldWidth;
                                var pl = -dx + oldX;
                                // pl = pl<0 ? 0 : pl;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                    height: dy + oldHeight,
                                    left: pl,
                                });
                                _scaleSameRatio($elemWrap, dx, dy);
                            }
                            else if (angleRegion == 2) {//err
                                //dx = e.pageX - oldMouseX;
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                var pl = dx + oldX;
                                // pl = pl<0 ? 0 : pl;
                                $elemWrap.css({
                                    width: -dx + oldWidth,
                                    height: -dy + oldHeight,
                                    left: pl,
                                    top:oldY + dx,
                                });
                                _scaleSameRatio($elemWrap, -dx, -dy);
                            }
                            else if (angleRegion == 3) {//ok
                                //dx = e.pageX - oldMouseX;
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                    height: dy + oldHeight,
                                    top:oldY - dy,
                                });
                                _scaleSameRatio($elemWrap, dx, dy);
                            }
                            else if (angleRegion == 4) {//err
                                //dx = e.pageX - oldMouseX;
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                    height: dy + oldHeight,
                                    top:oldY + dx/2,
                                });
                                _scaleSameRatio($elemWrap, dx, dy);
                            }
                            break;
                        case "right"://右
                            if (angleRegion == 1) {
                                //dx = e.pageX - oldMouseX;
                                dx = _this.getPageX(e) - oldMouseX;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                });
                                _scaleWidthRatio($elemWrap, dx);
                            }
                            else if (angleRegion == 2) {
                                dy =  _this.getPageY(e) - oldMouseY;
                                $elemWrap.css({
                                    width: dy + oldWidth,
                                    left: oldX - dy/2,
                                    top: oldY + dy/2,
                                });
                                _scaleWidthRatio($elemWrap, dy);
                            }
                            else if (angleRegion == 3) {
                                dx = _this.getPageX(e) - oldMouseX;
                                $elemWrap.css({
                                    width: -dx + oldWidth,
                                    left:oldX + dx,
                                });
                                _scaleWidthRatio($elemWrap, -dx);
                            }
                            else if (angleRegion == 4) {
                                dy =  _this.getPageY(e) - oldMouseY;
                                $elemWrap.css({
                                    width: -dy + oldWidth,
                                    left: oldX + dy/2,
                                    top: oldY + dy/2,
                                });
                                _scaleWidthRatio($elemWrap, -dy);
                            }

                            break;
                        case "bottom"://下
                            if (angleRegion == 1) {
                                dy = _this.getPageY(e) - oldMouseY;
                                $elemWrap.css({
                                    height: dy + oldHeight,
                                });
                                _scaleHeightRatio($elemWrap, dy);
                            }
                            else if (angleRegion == 2) {
                                dx = _this.getPageX(e) - oldMouseX;
                                $elemWrap.css({
                                    height: -dx + oldHeight,
                                    top:oldY + dx/2,
                                    left:oldX + dx/2,
                                });
                                _scaleHeightRatio($elemWrap, -dx);
                            }
                            else if (angleRegion == 3) {
                                dy = _this.getPageY(e) - oldMouseY;
                                $elemWrap.css({
                                    height: -dy + oldHeight,
                                    top:oldY + dy,
                                });
                                _scaleHeightRatio($elemWrap, -dy);
                            }
                            else if (angleRegion == 4) {
                                dx = _this.getPageX(e) - oldMouseX;
                                $elemWrap.css({
                                    height: dx + oldHeight,
                                    top:oldY - dx/2,
                                    left:oldX + dx/2,
                                });
                                _scaleHeightRatio($elemWrap, dx);
                            }
                            break;
                        case "right-bottom"://右下
                            if (angleRegion == 1) {
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                    height: dy + oldHeight,
                                });
                                _scaleSameRatio($elemWrap, dx, dy);
                            }
                            else if (angleRegion == 2) {//err
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: -dx + oldWidth,
                                    height: -dy + oldHeight,
                                    left:oldX + dx,
                                    top:oldY - dy/2,
                                });
                                _scaleSameRatio($elemWrap, -dx, -dy);
                            }
                            else if (angleRegion == 3) {//ok
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: -dx + oldWidth,
                                    height: -dy + oldHeight,
                                    left:oldX + dx,
                                    top: oldY + dy,
                                });
                                _scaleSameRatio($elemWrap, -dx, -dy);
                            }
                            else if (angleRegion == 4) {//err
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                    height: dy + oldHeight,
                                    left:oldX + dy/2,
                                    top: oldY - dx,
                                });
                                _scaleSameRatio($elemWrap, dx, dy);
                            }
                            break;
                        case "right-top"://右上
                            if (angleRegion == 1) {
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                    height: dy + oldHeight,
                                    top: -dy + oldY,
                                });
                                _scaleSameRatio($elemWrap, dx, dy);
                            }
                            else if (angleRegion == 2) { // err
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: dx + oldWidth,
                                    height: dy + oldHeight,
                                    // top:oldY + dy,
                                });
                                _scaleSameRatio($elemWrap, dx, dy);
                            }
                            else if (angleRegion == 3) { //ok
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: -dx + oldWidth,
                                    height: -dy + oldHeight,
                                    left: oldX + dx,
                                });
                                _scaleSameRatio($elemWrap, -dx, -dy);
                            }
                            else if (angleRegion == 4) {//err
                                dx = _this.getPageX(e) - oldMouseX;
                                dy = dx*oldHeight/oldWidth;
                                $elemWrap.css({
                                    width: -dx + oldWidth,
                                    height: -dy + oldHeight,
                                    top:oldY + dy,
                                    left:oldX + dx,
                                });
                                _scaleSameRatio($elemWrap, -dx, -dy);
                            }
                            break;
                        case "rotate"://旋转
                            var currX = _this.getPageX(e) - RX;
                            var currY = RY - _this.getPageY(e);
                            var newDeg = getDeg(currX, currY);
                            changeDeg = newDeg - oldPointDeg + oldoldangle;
                            changeDeg = (changeDeg < 0) ? ( changeDeg + 360 ) : changeDeg;
                            if (changeDeg >= 360) {
                                changeDeg = changeDeg % 360;
                            }
                            
                            $elemWrap.css("transform", "rotate("+(changeDeg)+"deg)");
                            $elemWrap.css("-webkit-transform", "rotate("+(changeDeg)+"deg)");
                            $elemWrap.css("-ms-transform", "rotate("+(changeDeg)+"deg)");
                            $elemWrap.css("-moz-transform", "rotate("+(changeDeg)+"deg)");
                            break;
                        default:
                            //参考线
                            // 单选操作
                            var $nodes = $elemWrap.parent().find('.elem-wrap');
                            var pl = _this.getPageX(e) - oldMouseX + oldX;
                            $elemWrap.css({
                                left: getOffset(pl, "left"),
                                top: getOffset(_this.getPageY(e) - oldMouseY + oldY, "top")
                            });
                            break;
                    }
                }else if(e.type === "keydown"){
                    if (self.activeElem !== null && $(e.target).parents("#helper-wrap").length == 0){
                        if (isMoveBykey == false) {
                            positionBeforMove.top = parseFloat(self.activeElem.css('top'));
                            positionBeforMove.left = parseFloat(self.activeElem.css('left'));
                        }
                        if (e.keyCode == 38){
                            isMoveBykey = true;
                            self.activeElem.css({top: function(index, value){
                                return getOffset(parseFloat(value)-1, "top");
                            }})
                        }else if(e.keyCode == 39){
                            isMoveBykey = true;
                            self.activeElem.css({left: function(index, value){
                                return getOffset(parseFloat(value)+1, "left");
                            }})
                        }else if(e.keyCode == 40){
                            isMoveBykey = true;
                            self.activeElem.css({top: function(index, value){
                                return getOffset(parseFloat(value)+1, "top");
                            }})
                        }else if(e.keyCode == 37){
                            isMoveBykey = true;
                            self.activeElem.css({left: function(index, value){
                                return getOffset(parseFloat(value)-1, "left");
                            }})
                        }
                    }
                }else if (e.type === 'keyup') {
                    if (self.activeElem !== null && $(e.target).parents("#helper-wrap").length == 0){
                        if (isMoveBykey == true) {
                            var positionAfterMove = {left:parseFloat(self.activeElem.css('left')), top:parseFloat(self.activeElem.css('top'))};
                            if(positionAfterMove.left!=positionBeforMove.left || positionAfterMove.top!=positionBeforMove.top){
                                var Command = undoRedo.getCommand("moveElement");
                                var dc = new Command();
                                var domId = self.activeElem.attr('id');
                                var element = elements.getConElement(domId);
                                dc.setChangeObject(slidePanel.getActiveSlide(), element, positionBeforMove, positionAfterMove);
                                undoRedo.insertUndoCommand(dc);
                            }
                            isMoveBykey = false;
                            positionBeforMove = {};
                        }
                    }
                }
            })
        }
	}

	contenteditor.prototype.setContentPosition = function(){
		var concontainerWidth = 720;
        var concontainerHeight = 410;
        var scrollableWidth = $('#scrollable-content').width();
        var scrollableHeight = $('#scrollable-content').height();
        var mleft = (scrollableWidth-concontainerWidth)/2;
        var mtop = (scrollableHeight-concontainerHeight)/2;
        this.contentBody.css('margin-left',mleft+'px');
        this.contentBody.css('margin-top',mtop+'px');
	}

	// 拖拽放入元素
	contenteditor.prototype.bindDragEvent = function(){
		var _this = this;
		this.contentBodyDom.ondragenter = function(){  //相当于onmouseover
            // this.style.background = 'green';
        };

        this.contentBodyDom.ondragleave = function(){  //相当于onmouseout
            // this.style.background = 'red';
        };

        this.contentBodyDom.ondrop = function(ev){
            // this.style.background = 'red';  
            var elemId = ev.dataTransfer.getData('elemId');
            var machineId = $('#'+elemId).attr('data-id');
            var targetElem = $('#'+elemId).find('.elem-wrap');
            var newId = _this.productlineId+'_'+machineId;
            _this.renderElem(targetElem,_this.productlineId,machineId);
            var elemLeft = _this.getPageX(ev) - $('#'+newId).width()/2;
            var elemTop = _this.getPageY(ev) - $('#'+newId).height()/2;
            var position = {'left':elemLeft,'top':elemTop};
            _this.setPosition($('#'+newId),position);
        }

        this.contentBodyDom.ondragover = function(ev){
            ev.preventDefault();  //阻止默认事件：元素就可以释放了
            // ev.dataTransfer.dropEffect = 'link';  //针对外部文件
        }
	}

    function getElementHelperHtml(){
        return $($('.edit-helper').prop('outerHTML')).css('display', '').prop('outerHTML');
    }

	contenteditor.prototype.renderElem = function(targetElem,productlineId,machineId,position){
		var newId = productlineId+'_'+machineId;
        var content = targetElem.clone(true).attr('id',newId);
        content.attr('pdlid',this.productlineId);
        content.attr('macid',machineId);
        content.find('.machineitem').remove(); 
        content.find('.machineelem').show();
        var helperdom = getElementHelperHtml();
        content.append(helperdom);
        var conElem = content.prop('outerHTML');
        $('#content-wrap').append(conElem);
        // this.setPosition($(conElem),position);
        
	}

	contenteditor.prototype.setPosition = function(elemDom,position, revisePosByDomSize) {
        var dom = elemDom,contentBody = $('#concontainer');
        if (position) {
            var pl = revisePosByDomSize ? position.left - dom.width()/2 : position.left;
            pl = pl<0 ? 0: pl;
            var tp = revisePosByDomSize ? position.top - dom.height()/2 : position.top;
            tp = tp<0 ? 0: tp;
            // var zdex = position.zIndex ? position.zIndex : slidePanel.getMaxZindex();
            dom.css({
                left: pl,
                top: tp,
                // zIndex: zdex
            });
        } else {
            var pl = contentBody.width()/2 - dom.width()/2;
            var tp = contentBody.height()/2 - dom.height()/2;
            dom.css({
                left: pl<0 ? 0: pl,
                top: tp<0 ? 0: tp,
                // zIndex: slidePanel.getMaxZindex()
            });
        }
    }

    contenteditor.prototype.hideRightClickMenu = function(){
		$('#rightclickmenu').find('.tbmitem').hide();
		$('#rightclickmenu').hide();
	}


	contenteditor.prototype.showRightClickMenu = function(){
		var _this = this;
		var rightClickMenu = $('#rightclickmenu');
		console.log(rightClickMenu);
		// rightClickMenu.show();
		console.log('999');
		rightClickMenu.find('.tbmitem').hide();
		// 上移一层
		rightClickMenu.find('.upzindex-btn').off('click').on('click', function(){
			var zindex = _this.dom.css("z-index");
			_this.hideRightClickMenu();
			if(zindex==slidePanel.getMaxZindex()){
				return;
			}
			var slide = slidePanel.mcobj.getSlide(slidePanel.activeSn);
			var upperslides = [];
			_this.dom.parent().find('[data-type="element"]').each(function(){
				var slzind = parseInt($(this).css("z-index"));
				if (slzind > zindex){
					upperslides.push({'zindex':slzind, 'dom':$(this)});
				}
			});
			if(upperslides.length == 0){
				return;
			}
			upperslides.sort(function(a, b){return a.zindex>b.zindex ? 1 : -1});
			upperslides[0].dom.css('z-index', zindex);
			_this.dom.css("z-index", upperslides[0].zindex);
		}).css('display', 'block');

		// 下移一层
		rightClickMenu.find('.downzindex-btn').off('click').on('click', function(){
			_this.hideRightClickMenu();
			var zindex = _this.dom.css("z-index");
			var slide = slidePanel.mcobj.getSlide(slidePanel.activeSn);
			var downslides = [];
			_this.dom.parent().find('[data-type="element"]').each(function(){
				var slzind = parseInt($(this).css("z-index"));
				if (slzind < zindex){
					downslides.push({'zindex':slzind, 'dom':$(this)});
				}
			});
			if(downslides.length == 0){
				return;
			}
			downslides.sort(function(a, b){return a.zindex>b.zindex ? -1 : 1});
			downslides[0].dom.css('z-index', zindex);
			_this.dom.css("z-index", downslides[0].zindex);
		}).css('display', 'block');

		// 置顶
		rightClickMenu.find('.upzindex2top-btn').off('click').on('click', function(){
			var zindex = _this.dom.css("z-index");
			_this.hideRightClickMenu();
			if(zindex==slidePanel.getMaxZindex()){
				return;
			}
			var slide = slidePanel.mcobj.getSlide(slidePanel.activeSn);
			var upperslides = [];
			_this.dom.parent().find('[data-type="element"]').each(function(){
				var slzind = parseInt($(this).css("z-index"));
				if (slzind > zindex){
					upperslides.push({'zindex':slzind, 'dom':$(this)});
				}
			});
			if(upperslides.length == 0){
				return;
			}
			upperslides.sort(function(a, b){return a.zindex>b.zindex ? -1 : 1});
			_this.dom.css("z-index", upperslides[0].zindex + 1);
		}).css('display', 'block');

		// 置底
		rightClickMenu.find('.downzindex2bottom-btn').off('click').on('click', function(){
			_this.hideRightClickMenu();
			var zindex = _this.dom.css("z-index");
			var slide = slidePanel.mcobj.getSlide(slidePanel.activeSn);
			var downslides = [];
			_this.dom.parent().find('[data-type="element"]').each(function(){
				var slzind = parseInt($(this).css("z-index"));
				$(this).css("z-index",slzind + 1);
				if (slzind < zindex){
					downslides.push({'zindex':slzind + 1, 'dom':$(this)});
				}
			});
			if(downslides.length == 0){
				return;
			}
			downslides.sort(function(a, b){return a.zindex>b.zindex ? 1 : -1});
			_this.dom.css("z-index", downslides[0].zindex -1);
		}).css('display', 'block');
		
		// 删除
		rightClickMenu.find('.remove-btn').off('click').on('click', function(){
			var Id = _this.dom.attr("id");
			var contentEditPanel = root.zneditor.contentEditPanel;
			contentEditPanel.removeElemFromDom(_this.dom);
			_this.hideRightClickMenu();
		}).off('mouseover').off('mouseout').css('display', 'block');
		
	}       
//右键菜单
    contenteditor.prototype.getPageX = function(ev){
        // var contentBody = $('#concontainer');
        var pageX = ev.pageX ? ev.pageX : ev.originalEvent.pageX;
        return pageX - this.contentBody.offset().left;
    }

    contenteditor.prototype.getPageY = function(ev){
        // var contentBody = $('#concontainer');
        var pageY = ev.pageY ? ev.pageY : ev.originalEvent.pageY;
        return pageY - this.contentBody.offset().top;
    }



	root.contenteditor = contenteditor;
	
}).call(this);